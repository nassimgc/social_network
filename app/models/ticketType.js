const mongoose = require('mongoose');
const GUID = require('mongoose-guid')(mongoose);
const uniqueValidator = require('mongoose-unique-validator');

const Schema = new mongoose.Schema({
    name: {
      type: String,
      required: [true, 'Name is required'],
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User',
        immutable: true
    },
    users: {
        type: [mongoose.Schema.Types.ObjectId],
        ref: 'User'
    },
    amount:{
        type: Number,
        required: [true, 'Amount is required'],
    },
    quantity:{
        type: Number,
        required: [true, 'Quantity is required'],
    },
    event:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Event',
        immutable: true
    }
}, {
    collection:'ticketType',
    // Bien optimiser la requête
    minimize: false,
    versionKey: false
}).set('toJSON',{
    transform: (doc, ret) => {
        // doc : Données qu'on récupère
        ret.id = ret._id;

        delete ret._id;
    }
});

Schema.plugin(uniqueValidator, { message: '{PATH} already exist' });

module.exports = Schema;