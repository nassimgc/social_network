const EventModel = require('../models/event.js');
const UserModel = require('../models/user.js');
const AlbumModel = require('../models/album.js');
const PictureModel = require('../models/picture.js');
const CommentModel = require('../models/comment.js');
const SondageModel = require('../models/sondage.js');
const QuestionModel = require('../models/question.js');
const AnswerModel = require('../models/answer.js');


module.exports = class Albums {
    constructor(app, connect) {
        this.app = app;
        this.EventModel = connect.model('Event', EventModel);
        this.UserModel = connect.model('User', UserModel);
        this.AlbumModel = connect.model('Album', AlbumModel);
        this.PictureModel = connect.model('Picture', PictureModel);
        this.CommentModel = connect.model('Comment', CommentModel);
        this.SondageModel = connect.model('Sondage', SondageModel);
        this.QuestionModel = connect.model('Question', QuestionModel);
        this.AnswerModel = connect.model('Answer', AnswerModel);
        
        this.run();
    }

    showAllSondages(){
        this.app.get('/sondages/all/show',async (req, res)=> {
            try{
                this.SondageModel.find({ }).populate({
                    path: 'event',
                    populate: [
                        { 
                        path: 'createdBy', 
                        select:'email -_id' 
                        },
                        { 
                        path: 'managers', 
                        select:'email -_id' 
                        },
                        { 
                        path: 'participants', 
                        select:'email -_id' 
                        },
                        { 
                            path: 'sondages', 
                            select:'title _id' 
                        },

                    ]
                })
                .populate({
                    path: 'createdBy',
                    select: 'email -_id'
                })
                .populate({
                    path: 'questions',
                    populate: [
                        { 
                        path: 'createdBy', 
                        select:'email -_id' 
                        }
                    ],
                    select: 'title'
                  })
                .then((sondage) => {
                    res.status(200).json(sondage || 'Sondage not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });
            } catch(err) {
                console.error(`[ERROR] get:sondage -> ${err}`);
            
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    createSondage(){
        this.app.post('/sondages/',async (req, res)=> {
            try{

                const userQuery = this.UserModel.findOne({ email: req.body.createdBy }).then((user) => {
                    return user;
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

                const user = await userQuery;
                if(!user){
                    res.status(404).json('User not found');
                    return;
                }
                req.body.createdBy = user._id;
                const eventQuery = this.EventModel.findOne({_id: req.body.event }).then((evenment)=>{
                        return evenment;
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });
                const event = await eventQuery;
                if(!event){
                    res.status(404).json(`Event not found`);
                    return;
                }

                if(!req.body.question){
                    res.status(404).json('You need to input a question');
                    return;
                }

                if(!event.managers.includes(user._id)){
                    res.status(403).json('You\'re not allowed create sondage in this event');
                    return;
                }
                let bodySondage = {
                    'title': req.body.title,
                    'event':event._id,
                    'createdBy': req.body.createdBy
                };
                const sondageModelQuery = new this.SondageModel(bodySondage);
                const sondageQuerySave = sondageModelQuery.save().then((sondage) => {
                    // res.status(200).json(discu || {});
                    return sondage
                }).catch((err)=>{  
                    return(err.message || {});
                });
                const sondage = await sondageQuerySave;
                if(sondage){
                    if(typeof sondage !== 'object' || !'_id' in sondage){
                        res.status(404).json(sondage || 'Sondage not found');
                        return;
                    }    
                }else{
                    res.status(404).json('Sondage not found');
                    return;
                }
                let bodyQuestion = {
                    'title': req.body.question,
                    'event': event._id,
                    'sondage':sondage._id,
                    'createdBy': req.body.createdBy
                }
                const questionModelQuery = new this.QuestionModel(bodyQuestion);
                const questionQuerySave = questionModelQuery.save().then((question) => {
                    // res.status(200).json(discu || {});
                    return question
                }).catch((err)=>{  
                    return(err.message || {});
                });

                const question = await questionQuerySave;
                if(question){
                    if(typeof question !== 'object' || !'_id' in question){
                        res.status(404).json(question || 'Question not found');
                        return;
                    }    
                }else{
                    res.status(404).json('Question not found');
                    return;
                }

                this.EventModel.findOneAndUpdate({_id: event._id}, 
                    {$addToSet: { sondages: sondage  }}, {
                    // On renvoit l'event avec les nouvelles valeurs
                    new: true,
                    // Applique les règles de validation du modèle (Event)
                    runValidators: true
                }).catch((err)=>{  
                        res.status(422).json(err.message || {});
                        return; 
                });

                this.SondageModel.findOneAndUpdate({_id: sondage._id}, 
                    {$addToSet: { questions: question  }}, {
                    // On renvoit le sondage avec les nouvelles valeurs
                    new: true,
                    // Applique les règles de validation du modèle (Sondage)
                    runValidators: true
                }).then((sondage) => {
                        res.status(200).json(sondage || {});
                        return;
                }).catch((err)=>{  
                        res.status(422).json(err.message || {});
                        return;
                });

            } catch(err) {
                console.error(`[ERROR] post:sondages -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    showSondage(){
        this.app.get('/sondages/:id/show',async (req, res)=> {
            try{
                this.SondageModel.findOne({ _id: req.params.id }).populate({
                    path: 'event',
                    populate: [
                        { 
                        path: 'createdBy', 
                        select:'email -_id' 
                        },
                        { 
                        path: 'managers', 
                        select:'email -_id' 
                        },
                        { 
                        path: 'participants', 
                        select:'email -_id' 
                        },
                        { 
                            path: 'sondages', 
                            select:'title _id' 
                        },

                    ]
                })
                .populate({
                    path: 'createdBy',
                    select: 'email -_id'
                })
                .populate({
                    path: 'questions',
                    populate: [
                        { 
                        path: 'createdBy', 
                        select:'email -_id' 
                        }
                    ],
                    select: 'title'
                  })
                .then((sondage) => {
                    res.status(200).json(sondage || 'Sondage not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });
            } catch(err) {
                console.error(`[ERROR] get:sondage -> ${err}`);
            
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }
    updateSondage(){
        this.app.put('/sondages/:id/update', (req, res) => {
            try {
                this.SondageModel.findOneAndUpdate({_id: req.params.id}, 
                    req.body, {
                    // On renvoit le sondage avec les nouvelles valeurs
                    new: true,
                    // Applique les règles de validation du modèle (Sondage)
                    runValidators: true
                }).then((sondage) => {
                        res.status(200).json(sondage || 'Sondage not found');
                }).catch((err)=>{  
                        res.status(422).json(err.message || {});
                });
            }
            catch (err) {
                console.error(`[ERROR] put:sondage -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    deleteSondage(){
        this.app.delete('/sondages/:id/remove', async (req, res) => {
            try {
                const sondageQuery = this.SondageModel.findOne({ _id: req.params.id })
                .then((sondage) => {
                    return sondage;
                }).catch((err)=>{  
                    // res.status(422).json(`Search sondage step : ${err.message}`  || {});
                    return err.message;
                });

                const sondage = await sondageQuery;
                if(sondage){
                    if(typeof sondage !== 'object' || !'_id' in sondage){
                        res.status(404).json(sondage || 'Sondage not found');
                        return;
                    }    
                }else{
                    res.status(404).json('Sondage not found');
                    return;
                }
                this.AnswerModel.deleteMany({ sondage: sondage._id  }).catch((err)=>{
                    res.status(422).json(`Remove answers of sondage: ${err.message}`  || {});
                    return;
                }); 
                this.QuestionModel.deleteMany({ sondage: sondage._id  }).catch((err)=>{
                    res.status(422).json(`Remove questions of sondage: ${err.message}`  || {});
                    return;
                });

                this.EventModel.findOneAndUpdate({_id: sondage.event}, 
                    {$pull: { sondages: sondage._id  }}, {
                    // On renvoit l'event avec les nouvelles valeurs
                    new: true,
                    // Applique les règles de validation du modèle (Event)
                    runValidators: true
                }).catch((err)=>{  
                        res.status(422).json(err.message || {});
                        return; 
                });

                this.SondageModel.deleteOne({_id: sondage._id}).then((d)=>{
                    if (d.acknowledged && d.deletedCount == 1){
                        res.status(200).json('Sondage successfully delete');
                        return;
                    }else{
                        res.status(200).json('Couldn\'t delete sondage');
                        return;
                    }
                }).catch((err)=>{
                    res.status(422).json(`Remove sondage: ${err.message}`  || {});
                    return;
                });
            }
            catch (err) {
                console.error(`[ERROR] delete:sondage -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    createQuestion(){
        this.app.post('/sondages/:id/question/add', async (req, res) => {
            try {
                const sondageQuery = this.SondageModel.findOne({ _id: req.params.id }).then((sondage) => {
                    return sondage;
                }).catch((err)=>{  
                    // res.status(422).json(err.message || {});
                });

                const sondage = await sondageQuery;
                if(!sondage){
                    res.status(404).json('Sondage not found');
                    return;
                }

                if(sondage){
                    if(typeof sondage !== 'object' || !'_id' in sondage){
                        res.status(404).json(sondage || 'Sondage not found');
                        return;
                    }    
                }else{
                    res.status(404).json('Sondage not found');
                    return;
                }
                const userQuery = this.UserModel.findOne({ email: req.body.createdBy }).then((user) => {
                    return user;
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                    return err.message || {}
                });

                const user = await userQuery;
                if(user){
                    if(typeof user !== 'object' || !'_id' in user){
                        res.status(404).json(user || 'User not found');
                        return;
                    }    
                }else{
                    res.status(404).json('User not found');
                    return;
                }

                const getEventQuery = this.EventModel.findOne({ _id: sondage.event }).then((event) => {
                    return event;
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                    return;
                });

                const event = await getEventQuery;
                if(!event.managers.includes(user._id)){
                    res.status(403).json('You\'re not allowed create question in this sondage');
                    return;
                }

                let body = {
                    'title':req.body.title,
                    'createdBy': user._id,
                    'sondage': sondage._id,
                    'event': sondage.event
                };
                const questionModelQuery = new this.QuestionModel(body);
                const questionQuerySave = questionModelQuery.save().then((question) => {
                    return question;
                }).catch((err)=>{  
                    return(err.message || {});
                });

                const question = await questionQuerySave;
                if(question){
                    if(typeof question !== 'object' || !'_id' in question){
                        res.status(404).json(question || 'Question not found');
                        return;
                    }    
                }else{
                    res.status(404).json('Question not found');
                    return;
                }

                this.SondageModel.findOneAndUpdate({_id: sondage._id}, 
                    {$push: { questions: question  }}, {
                    // On renvoit le sondage avec les nouvelles valeurs
                    new: true,
                    // Applique les règles de validation du modèle (Sondage)
                    runValidators: true
                }).then((sondage) => {
                        res.status(200).json(question || {});
                        return;
                }).catch((err)=>{  
                        res.status(422).json(err.message || {});
                        return;
                });

            }
            catch (err) {
                console.error(`[ERROR] post:question -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }
    showQuestion(){
        this.app.get('/sondages/:idSondage/question/:idQuestion/show', async (req, res) => {
            try {
                this.QuestionModel.findOne({ _id: req.params.idQuestion, sondage: req.params.idSondage })
                .populate({
                    path: 'answers',
                    select: 'title'
                    // select: 'title -_id'
                  })
                  .populate({
                    path: 'createdBy',
                    select: 'email'
                    // select: 'title -_id'
                  })
                  .populate({
                    path: 'sondage',
                    select: 'title'
                    // select: 'title -_id'
                  })
                  .populate({
                    path: 'event',
                    populate: [
                        { 
                        path: 'createdBy', 
                        select:'email -_id' 
                        },
                        { 
                        path: 'managers', 
                        select:'email -_id' 
                        },
                        { 
                        path: 'participants', 
                        select:'email -_id' 
                        },
                        { 
                            path: 'sondages', 
                            select:'title _id' 
                        },

                    ]
                })
                .then((question) => {
                    res.status(200).json(question || 'Question or sondage not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

            }
            catch (err) {
                console.error(`[ERROR] get:question -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    showQuestions(){
        this.app.get('/sondages/:id/questions/show', async (req, res) => {
            try {
                this.SondageModel.findOne({ _id: req.params.id }, {questions: 1, _id: 0})
                .populate({
                    path: 'questions',
                    select: 'title'
                    // select: 'title -_id'
                  })
                .then((sondage) => {
                    res.status(200).json(sondage || 'Sondage not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

            }
            catch (err) {
                console.error(`[ERROR] get:questions -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }
    updateQuestion(){
        this.app.put('/question/:id/update', (req, res) => {
            try {
                    // console.log(typeof req.body.managers);
                    this.QuestionModel.findOneAndUpdate({_id: req.params.id}, 
                        req.body, {
                        // On renvoit la question avec les nouvelles valeurs
                        new: true,
                        // Applique les règles de validation du modèle (Question)
                        runValidators: true
                    }).then((question) => {
                            res.status(200).json(question || 'Question not found');
                    }).catch((err)=>{  
                            res.status(422).json(err.message || {});
                    });
            }
            catch (err) {
                console.error(`[ERROR] put:question -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }
    deleteQuestion(){
        this.app.delete('/sondages/:idSondage/question/:idQuestion/remove', async (req, res) => {
            try {
                const questionQuery = this.QuestionModel.findOne({ _id: req.params.idQuestion })
                .then((question) => {
                    return question;
                }).catch((err)=>{  
                    // res.status(422).json(`Search question step : ${err.message}`  || {});
                    return err.message || {};
                });

                const question = await questionQuery;
                if(question){
                    if(typeof question !== 'object' || !'_id' in question){
                        res.status(404).json(question || 'Question not found');
                        return;
                    }    
                }else{
                    res.status(404).json('Question not found');
                    return;
                }
                const sondageQuery = this.SondageModel.findOneAndUpdate({_id: req.params.idSondage }, 
                    {   
                        $pull: {
                        questions: question._id,
                        }
                    }, 
                    {
                        // On renvoit le sondage avec les nouvelles valeurs
                        new: true,
                        // Applique les règles de validation du modèle (Sondage)
                        runValidators: true
                    }).then((sondage) => {
                            return sondage;
                    }).catch((err)=>{  
                       // res.status(422).json(`Remove question in sondage step : ${err.message}`  || {});
                       return err.message || {};
                    });

                    const sondage = await sondageQuery;

                    if(sondage){
                        if(typeof sondage !== 'object' || !'_id' in sondage){
                            res.status(404).json(sondage || 'Sondage not found');
                            return;
                        }    
                    }else{
                        res.status(404).json('Sondage not found');
                        return;
                    }

                    this.AnswerModel.deleteMany({ question: question._id }).catch((err)=>{
                        res.status(422).json(`Remove answers of question: ${err.message}`  || {});
                    });

                    this.QuestionModel.deleteOne({_id: question._id}).then((d)=>{
                        if (d.acknowledged && d.deletedCount == 1){
                            res.status(200).json('Question successfully delete');
                            return;
                        }else{
                            res.status(200).json('Couldn\'t delete question');
                            return;
                        }
                    }).catch((err)=>{
                        res.status(422).json(`Remove question: ${err.message}`  || {});
                        return;
                    });
            }
            catch (err) {
                console.error(`[ERROR] delete:question -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    createAnswer(){
        this.app.post('/sondages/:idSondage/question/:idQuestion/answer/add', async (req, res) => {
            try {
                const sondageQuery = this.SondageModel.findOne({ _id: req.params.idSondage }).then((sondage) => {
                    return sondage;
                }).catch((err)=>{  
                    // res.status(422).json(err.message || {});
                    return err.message || {};
                });

                const sondage = await sondageQuery;
                if(sondage){
                    if(typeof sondage !== 'object' || !'_id' in sondage){
                        res.status(404).json(sondage || 'Sondage not found');
                        return;
                    }    
                }else{
                    res.status(404).json('Sondage not found');
                    return;
                }

                const questionQuery = this.QuestionModel.findOne({ _id: req.params.idQuestion }).then((question) => {
                    return question;
                }).catch((err)=>{  
                    // res.status(422).json(err.message || {});
                    return err.message || {};
                });

                const question = await questionQuery;
                if(question){
                    if(typeof question !== 'object' || !'_id' in question){
                        res.status(404).json(question || 'Question not found');
                        return;
                    }    
                }else{
                    res.status(404).json('Question not found');
                    return;
                }

                const userQuery = this.UserModel.findOne({ email: req.body.createdBy }).then((user) => {
                    return user;
                }).catch((err)=>{  
                    // res.status(422).json(err.message || {});
                    return err.message || {};
                });

                const user = await userQuery;
                if(user){
                    if(typeof user !== 'object' || !'_id' in user){
                        res.status(404).json(user || 'User not found');
                        return;
                    }    
                }else{
                    res.status(404).json('User not found');
                    return;
                }

                const getEventQuery = this.EventModel.findOne({ _id: sondage.event }).then((event) => {
                    return event;
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                    return;
                });

                const event = await getEventQuery;
                if(!event.managers.includes(user._id)){
                    res.status(403).json('You\'re not allowed create answer in this question');
                    return;
                }

                let body = {
                    'title': req.body.title,
                    'createdBy': user._id,
                    'event': sondage.event,
                    'sondage': sondage._id,
                    'question':question._id
                };
                
                const answerModelQuery = new this.AnswerModel(body);
                const answerQuerySave = answerModelQuery.save().then((answer) => {
                    return answer;
                }).catch((err)=>{  
                    // res.status(422).json(err.message || {});
                    return(err.message || {});
                });

                const answer = await answerQuerySave;
                if(answer){
                    if(typeof answer !== 'object' || !'_id' in answer){
                        res.status(404).json(answer || 'Answer not found');
                        return;
                    }    
                }else{
                    res.status(404).json('Answer not found');
                    return;
                }

                this.QuestionModel.findOneAndUpdate({_id: question._id}, 
                    {$push: { answers: answer  }}, {
                    // On renvoit l'utilisateur avec les nouvelles valeurs
                    new: true,
                    // Applique les règles de validation du modèle (User)
                    runValidators: true
                }).then(() => {
                        res.status(200).json(answer || {});
                        return;
                }).catch((err)=>{  
                        res.status(422).json(err.message || {});
                        return;
                });
            }
            catch (err) {
                console.error(`[ERROR] post:answer -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }
    giveAnswer(){
        this.app.post('/sondages/:idSondage/question/:idQuestion/answer/give', async (req, res) => {
            try {
                const sondageQuery = this.SondageModel.findOne({ _id: req.params.idSondage }).then((sondage) => {
                    return sondage;
                }).catch((err)=>{  
                    // res.status(422).json(err.message || {});
                    return err.message || {};
                });

                const sondage = await sondageQuery;
                if(sondage){
                    if(typeof sondage !== 'object' || !'_id' in sondage){
                        res.status(404).json(sondage || 'Sondage not found');
                        return;
                    }    
                }else{
                    res.status(404).json('Sondage not found');
                    return;
                }

                const questionQuery = this.QuestionModel.findOne({ _id: req.params.idQuestion }).then((question) => {
                    return question;
                }).catch((err)=>{  
                    // res.status(422).json(err.message || {});
                    return err.message || {};
                });

                const question = await questionQuery;
                if(question){
                    if(typeof question !== 'object' || !'_id' in question){
                        res.status(404).json(question || 'Question not found');
                        return;
                    }    
                }else{
                    res.status(404).json('Question not found');
                    return;
                }

                const userQuery = this.UserModel.findOne({ email: req.body.giveBy }).then((user) => {
                    return user;
                }).catch((err)=>{  
                    // res.status(422).json(err.message || {});
                    return err.message || {};
                });

                const user = await userQuery;
                if(user){
                    if(typeof user !== 'object' || !'_id' in user){
                        res.status(404).json(user || 'User not found');
                        return;
                    }    
                }else{
                    res.status(404).json('User not found');
                    return;
                }

                const getEventQuery = this.EventModel.findOne({ _id: sondage.event }).then((event) => {
                    return event;
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                    return;
                });

                const event = await getEventQuery;
                if(!event.managers.includes(user._id) || !event.participants.includes(user._id)){
                    res.status(403).json('You\'re not allowed give answer in this question');
                    return;
                }

                // On check si l'utilisateur a déjà répondu à la question
                const answerQuery = this.AnswerModel.find({ users: user._id, question: question._id }).then((answers) => {
                    return answers;
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });
                const answers = await answerQuery;
                if(answers.length > 0){
                    res.status(200).json('User already answer this question');
                    return;
                }

                this.AnswerModel.findOneAndUpdate({_id: req.body.answerId}, 
                    { $addToSet: { users: user._id } }, {
                    // On renvoit l'event avec les nouvelles valeurs
                    new: true,
                    // Applique les règles de validation du modèle (Event)
                    runValidators: true
                }).then((answer) => {
                        res.status(200).json(answer || 'Answer not found');
                }).catch((err)=>{  
                    res.status(422).json(`Add Answer step : ${err.message}` || {});
                });
            }
            catch (err) {
                console.error(`[ERROR] give:question -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    showAnswersQuestion(){
        this.app.get('/sondages/:idSondage/question/:idQuestion/answers', async (req, res) => {
            try {
                this.SondageModel.findOne({ _id: req.params.idSondage }, {createdBy: 0, _id: 0, questions: {$elemMatch: {$eq: req.params.idQuestion}}})
                .populate({
                    path: 'questions',
                    populate: [
                        { 
                            path: 'answers',
                            populate:[
                                {
                                    path:'createdBy',
                                    select:'email -_id'
                                },
                                {
                                    path:'users',
                                    select:'email -_id'
                                },
                            ],
                            select:'title createdBy _id' 
                            // select:'content createdBy -_id' 
                        },
                    ],
                    select: 'title -_id'
                  })
                .then((sondage) => {
                    res.status(200).json(sondage || 'Sondage or question not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

            }
            catch (err) {
                console.error(`[ERROR] get:answers -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }
    updateAnswer(){
        this.app.put('/answer/:id/update', (req, res) => {
            try {
                    // console.log(typeof req.body.managers);
                    this.AnswerModel.findOneAndUpdate({_id: req.params.id}, 
                        req.body, {
                        // On renvoit answer avec les nouvelles valeurs
                        new: true,
                        // Applique les règles de validation du modèle (Answer)
                        runValidators: true
                    }).then((answer) => {
                            res.status(200).json(answer || 'Answer not found');
                    }).catch((err)=>{  
                            res.status(422).json(err.message || {});
                    });
            }
            catch (err) {
                console.error(`[ERROR] put:answer -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    deleteAnswer(){
        this.app.delete('/sondage/:idSondage/question/:idQuestion/answer/:idAnswer/remove', async (req, res) => {
            try {
                const answerQuery = this.AnswerModel.findOne({ _id: req.params.idAnswer })
                .then((answer) => {
                    return answer;
                }).catch((err)=>{  
                    // res.status(422).json(`Search answer step : ${err.message}`  || {});
                    return err.message || {};
                });
                const answer = await answerQuery;
                if(answer){
                    if(typeof answer !== 'object' || !'_id' in answer){
                        res.status(404).json(answer || 'Answer not found');
                        return;
                    }    
                }else{
                    res.status(404).json('Answer not found');
                    return;
                }

                const questionQuery = this.QuestionModel.findOneAndUpdate({_id: req.params.idQuestion, sondage: req.params.idSondage }, 
                    {   
                        $pull: {
                        answers: answer._id,
                        }
                    }, 
                    {
                        // On renvoit la question avec les nouvelles valeurs
                        new: true,
                        // Applique les règles de validation du modèle (Question)
                        runValidators: true
                    }).then((question) => {
                            return question;
                    }).catch((err)=>{  
                        // res.status(422).json(`Remove answer in question step : ${err.message}`  || {});
                        return err.message || {};
                    });

                const question = await questionQuery;
                if(question){
                    if(typeof question !== 'object' || !'_id' in question){
                        res.status(404).json(question || 'Question not found');
                        return;
                    }    
                }else{
                    res.status(404).json('Question not found');
                    return;
                }

                this.AnswerModel.deleteOne({_id: answer._id}).then((d)=>{
                    if (d.acknowledged && d.deletedCount == 1){
                        res.status(200).json('Answer successfully delete');
                        return;
                    }else{
                        res.status(200).json('Couldn\'t delete answer');
                        return;
                    }
                }).catch((err)=>{
                    res.status(422).json(`Remove answer: ${err.message}`  || {});
                    return;
                });
            }
            catch (err) {
                console.error(`[ERROR] delete:answer -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    showAnswer(){
        this.app.get('/sondages/:idSondage/question/:idQuestion/answer/:idAnswer', async (req, res) => {
            try {
                this.AnswerModel.findOne({ _id: req.params.idAnswer, sondage: req.params.idSondage, question: req.params.idQuestion})
                .populate({
                    path: 'users',
                    select: 'email '
                    // select: 'email -_id'
                  })
                  .populate({
                    path: 'createdBy',
                    select: 'email'
                    // select: 'email -_id'
                  })
                  .populate({
                    path: 'sondage',
                    select: 'title'
                    // select: 'title -_id'
                  })
                  .populate({
                    path: 'question',
                    select: 'title'
                    // select: 'title -_id'
                  })
                .then((answer) => {
                    res.status(200).json(answer || 'Answer or question or sondage not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

            }
            catch (err) {
                console.error(`[ERROR] get:answer -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
        
    }

    run() {
        // Sondage
        this.createSondage();
        this.showAllSondages();
        this.showSondage();
        this.updateSondage();
        this.deleteSondage();

        // Question
        this.createQuestion();
        this.showQuestion();
        this.showQuestions();
        this.updateQuestion();
        this.deleteQuestion();

        this.showAnswersQuestion();

        // Answers
        this.createAnswer();
        this.showAnswer();
        this.giveAnswer();
        this.updateAnswer();
        this.deleteAnswer();
    }
}