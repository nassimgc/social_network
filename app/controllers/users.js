const UserModel = require('../models/user.js');
const EventModel = require('../models/event.js');
const GroupModel = require('../models/group.js');
const DiscussionModel = require('../models/discussion.js');
const MessageModel = require('../models/message.js');
const CommentModel = require('../models/comment.js');
const AlbumModel = require('../models/album.js');
const PictureModel = require('../models/picture.js');
const SondageModel = require('../models/sondage.js');
const QuestionModel = require('../models/question.js');
const AnswerModel = require('../models/answer.js');

module.exports = class Users {
    constructor(app, connect) {
        this.app = app;
        this.UserModel = connect.model('User', UserModel);
        this.EventModel = connect.model('Event', EventModel);
        this.GroupModel = connect.model('Group', GroupModel);
        this.DiscussionModel = connect.model('Discussion', DiscussionModel);
        this.MessageModel = connect.model('Message', MessageModel);
        this.CommentModel = connect.model('Comment', CommentModel);
        this.AlbumModel = connect.model('Album', AlbumModel);
        this.PictureModel = connect.model('Picture', PictureModel);
        this.SondageModel = connect.model('Sondage', SondageModel);
        this.QuestionModel = connect.model('Question', QuestionModel);
        this.AnswerModel = connect.model('Answer', AnswerModel);
        this.run();
    }

    showAllUsers(){
        this.app.get('/users/all/show',async (req, res)=> {
            try{
                this.UserModel.find({}, {password: 0}).then((users) => {
                    res.status(200).json(users || 'Users not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });
            } catch(err) {
                console.error(`[ERROR] get: All users -> ${err}`);
            
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }
    addUser(){
        this.app.post('/users/',async (req, res)=> {
            try{
                const userModelQuery = new this.UserModel(req.body);

                userModelQuery.save().then((user) => {
                    res.status(200).json(user || {});
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

            } catch(err) {
                console.error(`[ERROR] post:users -> ${err}`);
            
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }
    showUser(){
        this.app.get('/users/:id/show',async (req, res)=> {
            try{
                this.UserModel.findOne({ _id: req.params.id }, {password: 0}).then((user) => {
                    res.status(200).json(user || 'User not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });
            } catch(err) {
                console.error(`[ERROR] get:users -> ${err}`);
            
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });

        // this.app.get('/users/tok/:token',async (req, res)=> {
        //     try{
        //         console.log(req.params.token);
        //         this.UserModel.findOne({ token: req.params.token }).then((user) => {
        //             res.status(200).json(user || 'User not found');
        //         }).catch((err)=>{  
        //             res.status(422).json(err.message || {});
        //         });
        //     } catch(err) {
        //         console.error(`[ERROR] get:users -> ${err}`);
            
        //         res.status(400).json({
        //             code: 400,
        //             message: `${err}`
        //         });
        //     }
        // });
    }

    updateUser(){
        this.app.put('/users/:id/update', (req, res) => {
            try {
                    this.UserModel.findOneAndUpdate({_id: req.params.id}, 
                        req.body, {
                        // On renvoit l'utilisateur avec les nouvelles valeurs
                        new: true,
                        // Applique les règles de validation du modèle (User)
                        runValidators: true
                    }).then((user) => {
                            res.status(200).json(user || 'User not found');
                    }).catch((err)=>{  
                            res.status(422).json(err.message || {});
                    });
            }
            catch (err) {
                console.error(`[ERROR] put:users -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    deleteUser(){
        this.app.delete('/users/:id/remove', async (req, res) => {
            try {
                const userQuery = this.UserModel.findOne({ _id: req.params.id })
                .then((user) => {
                    return user;
                }).catch((err)=>{  
                    res.status(422).json(`Search group step : ${err.message}`  || {});
                });

                const user = await userQuery;

                if(!user){
                    res.status(404).json('User not found');
                    return;
                }

                this.GroupModel.updateMany({$or:[{admins: user._id}, {members: user._id}]}, {
                    $pull: {
                        admins: user._id,
                        members: user._id
                    }
                });
                this.EventModel.updateMany({$or:[{managers: user._id}, {participants: user._id}]}, {
                    $pull: {
                        managers: user._id,
                        participants: user._id
                    }
                });
                this.PictureModel.deleteMany({ createdBy: user._id }).catch((err)=>{
                    res.status(422).json(`Remove pictures of user: ${err.message}`  || {});
                    return;
                }); 

                this.MessageModel.deleteMany({ createdBy: user._id }).catch((err)=>{
                    res.status(422).json(`Remove messages of user: ${err.message}`  || {});
                    return;
                });

                this.CommentModel.deleteMany({  createdBy: user._id }).catch((err)=>{
                    res.status(422).json(`Remove comments of user: ${err.message}`  || {});
                }); 

                this.AnswerModel.deleteMany({ createdBy: user._id }).catch((err)=>{
                    res.status(422).json(`Remove answers of user: ${err.message}`  || {});
                }); 

                this.QuestionModel.deleteMany({  createdBy: user._id }).catch((err)=>{
                    res.status(422).json(`Remove questions of user: ${err.message}`  || {});
                }); 

                this.SondageModel.deleteMany({  createdBy: user._id }).catch((err)=>{
                    res.status(422).json(`Remove sondages of user: ${err.message}`  || {});
                }); 

                this.UserModel.deleteOne({_id: user._id}).then((d)=>{
                    if (d.acknowledged && d.deletedCount == 1){
                        res.status(200).json('User successfully delete');
                    }else{
                        res.status(200).json('Couldn\'t delete user');
                    }
                }).catch((err)=>{
                    res.status(422).json(`Remove user: ${err.message}`  || {});
                });
            }
            catch (err) {
                console.error(`[ERROR] delete:users -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    run() {
        this.addUser();
        this.showAllUsers();
        this.showUser();
        this.updateUser();
        this.deleteUser();
    }
}