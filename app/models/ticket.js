const mongoose = require('mongoose');
const GUID = require('mongoose-guid')(mongoose);
const uniqueValidator = require('mongoose-unique-validator');

const Schema = new mongoose.Schema({
    type: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'TicketType',
        immutable: true
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User',
        immutable: true
    },
    address: {
        type: String,
        required: [true, 'Address is required'],
    },
    buyDate:{
        type: Date,
        default: Date.now
    },
    event:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Event',
        immutable: true
    }
}, {
    collection:'ticket',
    // Bien optimiser la requête
    minimize: false,
    versionKey: false
}).set('toJSON',{
    transform: (doc, ret) => {
        // doc : Données qu'on récupère
        ret.id = ret._id;

        delete ret._id;
    }
});

Schema.plugin(uniqueValidator, { message: '{PATH} already exist' });

module.exports = Schema;