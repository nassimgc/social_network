const mongoose = require('mongoose');
const GUID = require('mongoose-guid')(mongoose);
const uniqueValidator = require('mongoose-unique-validator');
const UserModel = require('./user.js');

const Schema = new mongoose.Schema({
    type: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, 'Type is required'],
        refPath: 'onModel',
        immutable: true
    },
    onModel: {
      type: String,
      required: [true, 'Model is required'],
      enum: ['Event', 'Group'],
      immutable: true
    },
    messages: {
        type:[mongoose.Schema.Types.ObjectId],
        ref:'Message'
    },
    createdBy: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User', 
        required: true,
        immutable: true
     }
}, {
    collection:'discussions',
    // Bien optimiser la requête
    minimize: false,
    versionKey: false
}).set('toJSON',{
    transform: (doc, ret) => {
        // doc : Données qu'on récupère
        ret.id = ret._id;

        delete ret._id;
    }
});

Schema.plugin(uniqueValidator, { message: '{PATH} already exist' });

module.exports = Schema;