const UserModel = require('../models/user.js');
const EventModel = require('../models/event.js');
const GroupModel = require('../models/group.js');
const DiscussionModel = require('../models/discussion.js');
const MessageModel = require('../models/message.js');
const CommentModel = require('../models/comment.js');
const AlbumModel = require('../models/album.js');
const PictureModel = require('../models/picture.js');
const SondageModel = require('../models/sondage.js');
const QuestionModel = require('../models/question.js');
const AnswerModel = require('../models/answer.js');

module.exports = class Events {
    constructor(app, connect) {
        this.app = app;
        this.UserModel = connect.model('User', UserModel);
        this.EventModel = connect.model('Event', EventModel);
        this.GroupModel = connect.model('Group', GroupModel);
        this.DiscussionModel = connect.model('Discussion', DiscussionModel);
        this.MessageModel = connect.model('Message', MessageModel);
        this.CommentModel = connect.model('Comment', CommentModel);
        this.AlbumModel = connect.model('Album', AlbumModel);
        this.PictureModel = connect.model('Picture', PictureModel);
        this.SondageModel = connect.model('Sondage', SondageModel);
        this.QuestionModel = connect.model('Question', QuestionModel);
        this.AnswerModel = connect.model('Answer', AnswerModel);
        this.run();
    }

    showAllEvents(){
        this.app.get('/events/all/show',async (req, res)=> {
            try{
                this.EventModel.find({})
                .populate({
                    path: 'createdBy',
                    select: 'email'
                })
                .populate({
                    path: 'managers',
                    select: 'email'
                })
                .populate({
                    path: 'participants',
                    select: 'email'
                })
                .populate({
                    path: 'sondages',
                    select: 'title'
                })
                .populate('group')
                .then((event) => {
                    res.status(200).json(event || 'Event not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });
            } catch(err) {
                console.error(`[ERROR] get:group -> ${err}`);
            
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }
    createEvent(){
        this.app.post('/events/',async (req, res)=> {
            try{
                const userQuery = this.UserModel.findOne({ email: req.body.createdBy }).then((user) => {
                    return user
                }).catch((err)=>{  
                    res.status(422).json(`Search user step : ${err.message}` || {});
                });

                const user = await userQuery;
                if(!user){
                    res.status(404).json('User not found');
                    return;
                }
                let group = null;
                if(req.body.group){
                    const groupQuery = this.GroupModel.findOne({ _id: req.body.group }).then((group) => {
                        return group
                    }).catch((err)=>{  
                        res.status(422).json(`Search user step : ${err.message}` || {});
                    });
                    group = await groupQuery;
                    if(!group){
                        res.status(404).json('Group not found');
                        return;
                    }
                }
                req.body.createdBy = user._id;
                if(!req.body.managers){
                    req.body.managers = [];
                }
                if(!req.body.members){
                    req.body.participants = [];
                }
                req.body.managers.push(user._id);
                req.body.participants.push(user._id);

                // S'il y a un group on ajoute automatique la liste des participants/admins dans l'événement
                if(group){
                    if(group.admins.length > 0){
                        group.admins.forEach((admin) =>{
                            const index = req.body.participants.findIndex(object => object.toString() === admin.toString());
                            if (index === -1) {
                            console.log('Il est passé admin');
                            req.body.participants.push(admin);
                            }    
                        })
                    }
                    if(group.members.length > 0){
                        group.members.forEach((member) =>{
                            const index = req.body.participants.findIndex(object => object.toString() === member.toString());
                            if (index === -1) {
                            console.log('Il est passé member');
                            req.body.participants.push(member);
                            }
                        })
                    }
                }
                const eventModelQuery = new this.EventModel(req.body);
                let errors = eventModelQuery.validateSync();
                if(errors){
                    // throw new Error(errors);
                    res.status(422).json(`Error validation event step : ${errors.message}` || {});
                    return;
                }
               eventModelQuery.save().then((event) => {
                    if(group){
                        this.GroupModel.updateOne({_id: group._id}, 
                            { $addToSet: { events: event._id } }
                        ).catch((err)=>{  
                            res.status(422).json(`Add event to group step : ${err.message}`  || {});
                        });
                    }
                    res.status(200).json(event || {});
                }).catch((err)=>{  
                    res.status(422).json(`Save events step : ${err.message}` || {});
                });

            } catch(err) {
                console.error(`[ERROR] post:event -> ${err}`);
            
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }
    
    showEvent(){
        this.app.get('/events/:id/show',async (req, res)=> {
            try{
                this.EventModel.findOne({ _id: req.params.id })
                .populate({
                    path: 'createdBy',
                    select: 'email'
                })
                .populate({
                    path: 'managers',
                    select: 'email'
                })
                .populate({
                    path: 'participants',
                    select: 'email'
                })
                .populate({
                    path: 'sondages',
                    select: 'title'
                })
                .populate('group')
                .then((event) => {
                    res.status(200).json(event || 'Event not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });
            } catch(err) {
                console.error(`[ERROR] get:group -> ${err}`);
            
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    updateEvent(){
        this.app.put('/events/:id/update', async (req, res) => {
            try {
                    // console.log(typeof req.body.managers);
                    this.EventModel.findOneAndUpdate({_id: req.params.id}, 
                        req.body, {
                        // On renvoit l'event avec les nouvelles valeurs
                        new: true,
                        // Applique les règles de validation du modèle (Event)
                        runValidators: true
                    }).then((event) => {
                            res.status(200).json(event || 'Event not found');
                    }).catch((err)=>{  
                            res.status(422).json(err.message || {});
                    });
            }
            catch (err) {
                console.error(`[ERROR] put:event -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    deleteEvent(){
        this.app.delete('/events/:id/delete', async (req, res) => {
            try {
                    const eventQuery = this.EventModel.findOne({ _id: req.params.id })
                    .then((event) => {
                        return event;
                    }).catch((err)=>{  
                        res.status(422).json(`Search event step : ${err.message}`  || {});
                    });

                    const event = await eventQuery;

                    if(!event){
                        res.status(404).json('Event not found');
                        return;
                    }
                    this.DiscussionModel.deleteMany({ type: event._id }).catch((err)=>{
                        res.status(422).json(`Remove discussion of event: ${err.message}`  || {});
                        return;
                    }); 
                    this.AlbumModel.deleteMany({ event: event._id  }).catch((err)=>{
                        res.status(422).json(`Remove albums of event: ${err.message}`  || {});
                        return;
                    });
                    this.MessageModel.deleteMany({ event: event._id  }).catch((err)=>{
                        res.status(422).json(`Remove messages of event: ${err.message}`  || {});
                        return;
                    });
                    this.PictureModel.deleteMany({ event: event._id  }).catch((err)=>{
                        res.status(422).json(`Remove pictures of event: ${err.message}`  || {});
                        return;
                    });

                    this.CommentModel.deleteMany({ event: event._id }).catch((err)=>{
                        res.status(422).json(`Remove comments of event: ${err.message}`  || {});
                    }); 

                    this.AnswerModel.deleteMany({ event: event._id }).catch((err)=>{
                        res.status(422).json(`Remove answers of event: ${err.message}`  || {});
                    }); 

                    this.QuestionModel.deleteMany({ event: event._id }).catch((err)=>{
                        res.status(422).json(`Remove questions of event: ${err.message}`  || {});
                    }); 

                    this.SondageModel.deleteMany({ event: event._id }).catch((err)=>{
                        res.status(422).json(`Remove sondages of event: ${err.message}`  || {});
                    }); 

                    this.GroupModel.updateMany({events : { $in : [event._id]}}, 
                        {$pull: {events: event._id} }, {
                        // On renvoit l'event avec les nouvelles valeurs
                        new: true,
                        // Applique les règles de validation du modèle (Event)
                        runValidators: true
                    }).catch((err)=>{  
                        res.status(422).json(`remove event in groups step : ${err.message}` || {});
                        return;
                    });
                    this.EventModel.deleteOne({_id: event._id}).then((d)=>{
                        if (d.acknowledged && d.deletedCount == 1){
                            res.status(200).json('Event successfully delete');
                        }else{
                            res.status(200).json('Couldn\'t delete event');
                        }
                    }).catch((err)=>{
                        res.status(422).json(`Remove event: ${err.message}`  || {});
                    });
            }
            catch (err) {
                console.error(`[ERROR] delete:event -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    addParticipant(){
        this.app.put('/events/:id/participant/add', async (req, res) => {
            try {
                const userQuery = this.UserModel.findOne({ email: req.body.participant }).then((user) => {
                    return user;
                }).catch((err)=>{  
                    res.status(422).json(`Search user step : ${err.message}`  || {});
                });

                const user = await userQuery;

                if(!user){
                    res.status(404).json('User not found');
                    return;
                }
                // delete req.body.participant;
                // req.body.participants = user._id;
                this.EventModel.findOneAndUpdate({_id: req.params.id}, 
                    { $addToSet: { participants: user._id } }, {
                    // On renvoit l'event avec les nouvelles valeurs
                    new: true,
                    // Applique les règles de validation du modèle (Event)
                    runValidators: true
                }).then((event) => {
                        res.status(200).json(event || 'Event not found');
                }).catch((err)=>{  
                    res.status(422).json(`Add participant step : ${err.message}` || {});
                });
            }
            catch (err) {
                console.error(`[ERROR] put: add participant -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    showParticipants(){
        this.app.get('/events/:id/participants/show', async (req, res) => {
            try {
                this.EventModel.findOne({ _id: req.params.id }, {participants: 1, _id: 0})
                .populate({
                    path: 'participants',
                    select: 'email'
                  })
                .then((event) => {
                    res.status(200).json(event || 'Event not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

            }
            catch (err) {
                console.error(`[ERROR] get:participants -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    removeParticipant(){
        this.app.delete('/events/:id/participant/remove', async (req, res) => {
            try {
                const userQuery = this.UserModel.findOne({ email: req.body.participant }).then((user) => {
                    return user;
                }).catch((err)=>{  
                    res.status(422).json(`Search user step : ${err.message}`  || {});
                });

                const user = await userQuery;
                if(!user){
                    res.status(404).json('User not found');
                    return;
                }
                this.EventModel.findOneAndUpdate({_id: req.params.id}, 
                    {   
                        $pull: {
                        participants: user._id,
                        }
                    }, 
                    {
                        // On renvoit l'utilisateur avec les nouvelles valeurs
                        new: true,
                        // Applique les règles de validation du modèle (User)
                        runValidators: true
                    }).then((event) => {
                            res.status(200).json(event || 'Event not found');
                    }).catch((err)=>{  
                        res.status(422).json(`Remove participant step : ${err.message}`  || {});
                    });
            }
            catch (err) {
                console.error(`[ERROR] delete: remove participant -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    addManager(){
        this.app.put('/events/:id/manager/add', async (req, res) => {
            try {
                const userQuery = this.UserModel.findOne({ email: req.body.manager }).then((user) => {
                    return user;
                }).catch((err)=>{  
                    res.status(422).json(`Search user step : ${err.message}`  || {});
                });

                const user = await userQuery;
                if(!user){
                    res.status(404).json('User not found');
                    return;
                }
                // delete req.body.manager;
                // req.body.managers = user._id;
                this.EventModel.findOneAndUpdate({_id: req.params.id}, 
                    { $addToSet: { managers: user._id } }, {
                    // On renvoit l'utilisateur avec les nouvelles valeurs
                    new: true,
                    // Applique les règles de validation du modèle (User)
                    runValidators: true
                }).then((event) => {
                        res.status(200).json(event || 'Event not found');
                }).catch((err)=>{  
                    res.status(422).json(`Add manager step : ${err.message}`  || {});
                });
            }
            catch (err) {
                console.error(`[ERROR] put:add manager -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    showManagers(){
        this.app.get('/events/:id/managers/show', async (req, res) => {
            try {
                this.EventModel.findOne({ _id: req.params.id }, {managers: 1, _id: 0})
                .populate({
                    path: 'managers',
                    select: 'email'
                  })
                .then((event) => {
                    res.status(200).json(event || 'Event not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

            }
            catch (err) {
                console.error(`[ERROR] get:managers -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    removeManager(){
        this.app.delete('/events/:id/manager/remove', async (req, res) => {
            try {
                const userQuery = this.UserModel.findOne({ email: req.body.manager }).then((user) => {
                    return user;
                }).catch((err)=>{  
                    res.status(422).json(`Search user step : ${err.message}`  || {});
                });

                const user = await userQuery;
                if(!user){
                    res.status(404).json('User not found');
                    return;
                }
                this.EventModel.findOneAndUpdate({_id: req.params.id}, 
                    {   
                        $pull: {
                        managers: user._id,
                        }
                    }, 
                    {
                        // On renvoit l'utilisateur avec les nouvelles valeurs
                        new: true,
                        // Applique les règles de validation du modèle (User)
                        runValidators: true
                    }).then((event) => {
                            res.status(200).json(event || 'Event not found');
                    }).catch((err)=>{  
                        res.status(422).json(`Remove manager step : ${err.message}`  || {});
                    });
            }
            catch (err) {
                console.error(`[ERROR] delete: remove manager -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }
    run() {
        // Event
        this.createEvent();
        this.showAllEvents();
        this.showEvent();
        this.updateEvent();
        this.deleteEvent();
        // Participants
        this.addParticipant();
        this.showParticipants();
        this.removeParticipant();
        // Managers
        this.addManager();
        this.showManagers();
        this.removeManager();
    }
}