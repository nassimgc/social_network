
# My social network

An API building platform built with Express, MongoDB and Mongoose to manage data for **the social network Facebook.**

Below is the list of API models where each model have their own CRUD :

*   Album
*   Answer
*   Comment
*   Discussion
*   Event
*   Group
*   Message
*   Picture
*   Question
*   Sondage
*   ticket
*   ticketType
*   User

<!--- If we have only one group/collection, then no need for the "ungrouped" heading -->



## Endpoints



## Comments



### 1. localhost:3000/comment/:idComment/update


## The update C*omment* endpoint :

You can **update** the **comment** of a message or a picture.

You have to give in the URL the object id of the comment and also a JSON body with the "content" field

After you send the request, the update would be applied on the comment.


***Endpoint:***

```bash
Method: PUT
Type: RAW
URL: localhost:3000/comment/626eb41a497d35609b242e49/update
```



***Body:***

```js        
{
    "content":"Update comment content"
}
```



## Groups

Users can create groups, invite members and other organizers.

Facebook allows you to create your secret, private or public group in order to create events by automatically inviting all members.

Indeed, in a group, the creation of events is simplified by allowing the invitation of all members in one click. Within a public group, event organizers can share their events on other social networks to help them communicate their events.



### 1. Members



***Endpoint:***

```bash
Method: 
Type: 
URL: 
```



### 2. Admins



***Endpoint:***

```bash
Method: 
Type: 
URL: 
```



### 3. localhost:3000/groups/:idGroup/show


## Show a Group informations :

This endpoint use a GET method.

You have to give in the URL the object id of the specific **Group.**

After you send the request, you will see the details of the specific Group like group name, members, admins, the author ...

If the group does not exist then you will see the message **"Group not found"**


***Endpoint:***

```bash
Method: GET
Type: 
URL: localhost:3000/groups/62744d73b42566583f16b6f5/show
```



### 4. localhost:3000/groups/


## Create a group :

This endpoint use a POST method.

You have to give in the body request the following informations :

StartFragment

*   "**name**": name of group,
*   "**description**": the description of your group,
*   "**type**": the type of your group (Public, Secret, Private),
*   "**createdBy**": the email of the user that will create the group
    

After you send the request, you will see the details of the new group you just created

If the information is incorrectly entered, you will get an error message.


***Endpoint:***

```bash
Method: POST
Type: RAW
URL: localhost:3000/groups/
```



***Body:***

```js        
{
    "name":"Group 1",
    "description":"A description",
    "type":"Public",
    "createdBy":"Nassim.bougtib@hotmail.fr"
}
```



### 5. localhost:3000/groups/:idGroup/update


## Update a group information :

This endpoint use a PUT method.

You can update the information of specific group by giving is id object in url parameter.

You could give in the body request the following informations :

*   "**name**": name of group,
*   "**description**": the description of your group,
    

After you send the request, you will see the new details of the group you just update.

If the information is incorrectly entered, you will get an error message.


***Endpoint:***

```bash
Method: PUT
Type: RAW
URL: localhost:3000/groups/62744d73b42566583f16b6f5/update
```



***Body:***

```js        
{
    "name":"Group 10"
}
```



### 6. localhost:3000/groups/:id/delete


## Delete a specific group :

You can remove a specific **group** with this endpoint by giving in the URL the object id of the group

When you delete a group, it would delete all comments, messages, pictures, albums, evens, threads which are linked to the group so be careful when you use this endpoint

After you send the request, your group would be deleted in the **group document in mongoDB and you will see the message "Group delete successfully"** or it could return a message **"Group not found"** if you give a wrong object ID.


***Endpoint:***

```bash
Method: DELETE
Type: 
URL: localhost:3000/groups/62744d73b42566583f16b6f5/delete
```



### 7. localhost:3000/groups/all/show


## Display all groups :

You can display all groups with this endpoint.

After you send the request, you will see the **all the groups which are stored inside the groups collection in Mongodb** with their informations or an empty array if you don't have stored yet a group inside the collection


***Endpoint:***

```bash
Method: GET
Type: 
URL: localhost:3000/groups/all/show
```



## Events

Users can create events, invite participants and other managers.

You have the choice to link your event to a group or not.



### 1. Managers



***Endpoint:***

```bash
Method: 
Type: 
URL: 
```



### 2. Participants



***Endpoint:***

```bash
Method: 
Type: 
URL: 
```



### 3. localhost:3000/events/


## Create an Event :

This endpoint use a POST method.

**You have to give in the body request the following informations :**

*   "name": The name of the event,
*   "description": The description of the event,
*   "visibility": The visibility of the event (Private, Public),
*   "createdBy": The author of the event,
    

**You can also give optional parameter :**

*   "group": The object Id of an existant **Group**
    

After you send the request, you will see the details of the new Event you just created

If the information is incorrectly entered, you will get an error message.


***Endpoint:***

```bash
Method: POST
Type: RAW
URL: localhost:3000/events/
```



***Body:***

```js        
{
    "name":"event 1",
    "description":"My event",
    "visibility":"Public",
    "createdBy":"Nassim.bougtib@hotmail.fr",
    "group":"62744d73b42566583f16b6f5"
}
```



### 4. localhost:3000/events/:idEvent/show


## Show an Event informations :

This endpoint use a GET method.

You have to give in the URL the object id of the specific **Event.**

After you send the request, you will see the details of the specific Event like event name, managers, participants, the author, the optional linked group, ...

If the event does not exist then you will see the message **"Event not found"**


***Endpoint:***

```bash
Method: GET
Type: 
URL: localhost:3000/events/6274524b3bd01723c54957fe/show
```



### 5. localhost:3000/events/:idEvent/update


## Update an Event informations :

This endpoint use a PUT method.

You can update the information of specific event by giving is id object in url parameter.

You could give in the body request the following informations :

*   "**name**": name of event,
*   "**description**": the description of your event,
    

After you send the request, you will see the new details of the event you just update.

If the information is incorrectly entered, you will get an error message.


***Endpoint:***

```bash
Method: PUT
Type: RAW
URL: localhost:3000/events/6274524b3bd01723c54957fe/update
```



***Body:***

```js        
{
    "name":"Nassim event"
}
```



### 6. localhost:3000/events/:idEvent/delete


## Delete a specific event :

You can remove a specific **event** with this endpoint by giving in the URL the object id of the event

When you delete a event, it would delete all comments, messages, pictures, albums, surveys, threads which are linked to the group so be careful when you use this endpoint. It will also remove the event inside the events of the linked group if it's linked to a group

After you send the request, your group would be deleted in the **events document in mongoDB and you will see the message "Event delete successfully"** or it could return a message **"Event not found"** if you give a wrong object ID.


***Endpoint:***

```bash
Method: DELETE
Type: 
URL: localhost:3000/events/6274524b3bd01723c54957fe/delete
```



### 7. localhost:3000/events/all/show


## Display all events :

You can display all events with this endpoint.

After you send the request, you will see the **all the events which are stored inside the events collection in Mongodb** with their informations or an empty array if you don't have stored yet a event inside the collection.


***Endpoint:***

```bash
Method: GET
Type: 
URL: localhost:3000/events/all/show
```



## Threads



### 1. Messages



***Endpoint:***

```bash
Method: 
Type: 
URL: 
```



### 2. Comments



***Endpoint:***

```bash
Method: 
Type: 
URL: 
```



### 3. localhost:3000/discussions


Add a discussion


***Endpoint:***

```bash
Method: POST
Type: RAW
URL: localhost:3000/discussions
```



***Body:***

```js        
{
    "onModel":"Event",
    "type":"6274524b3bd01723c54957fe",
    "createdBy":"Nassim.bougtib@hotmail.com1"
}
```



### 4. localhost:3000/discussions/:idDiscussion/show


Add a discussion


***Endpoint:***

```bash
Method: GET
Type: RAW
URL: localhost:3000/discussions/6274551226f6e6401081204e/show
```



***Body:***

```js        
{
    "onModel":"Event",
    "type":"626df2f7bc59b3158c4a3f61",
    "createdBy":"Nassim.bougtib@hotmail.com"
}
```



### 5. localhost:3000/discussions/:idDiscussion/remove


Add a discussion


***Endpoint:***

```bash
Method: DELETE
Type: RAW
URL: localhost:3000/discussions/6274551226f6e6401081204e/remove
```



### 6. localhost:3000/discussions/idDiscussion/update


Add a discussion


***Endpoint:***

```bash
Method: PUT
Type: RAW
URL: localhost:3000/discussions/6274551226f6e6401081204e/update
```



***Body:***

```js        
{
    "onModel":"Event",
    "type":"626df2f7bc59b3158c4a3f61",
    "createdBy":"Nassim.bougtib@hotmail.com"
}
```



### 7. localhost:3000/discussions/all/show



***Endpoint:***

```bash
Method: GET
Type: 
URL: localhost:3000/discussions/all/show
```



## Albums



### 1. Pictures



***Endpoint:***

```bash
Method: 
Type: 
URL: 
```



### 2. Comments



***Endpoint:***

```bash
Method: 
Type: 
URL: 
```



### 3. localhost:3000/albums


Add a discussion


***Endpoint:***

```bash
Method: POST
Type: RAW
URL: localhost:3000/albums
```



***Body:***

```js        
{
    "event":"6274524b3bd01723c54957fe",
    "createdBy":"Nassim.bougtib@hotmail.fr"
}
```



### 4. localhost:3000/albums/:idAlbum/show


Add a discussion


***Endpoint:***

```bash
Method: GET
Type: RAW
URL: localhost:3000/albums/62745ca3fdaeb553fb5e0952/show
```



***Body:***

```js        
{
    "onModel":"Event",
    "type":"626df2f7bc59b3158c4a3f61",
    "createdBy":"Nassim.bougtib@hotmail.com"
}
```



### 5. localhost:3000/albums/:idAlbum/remove


Add a discussion


***Endpoint:***

```bash
Method: DELETE
Type: RAW
URL: localhost:3000/albums/626ea8976596b0008ef2381f/remove
```



### 6. localhost:3000/albums/:idAlbum/update


Add a discussion


***Endpoint:***

```bash
Method: PUT
Type: RAW
URL: localhost:3000/albums/626e0333dc4a8d9dfe366fa5/update
```



***Body:***

```js        
{
    "onModel":"Event",
    "type":"626df2f7bc59b3158c4a3f61",
    "createdBy":"Nassim.bougtib@hotmail.com"
}
```



### 7. localhost:3000/albums/all/show



***Endpoint:***

```bash
Method: GET
Type: 
URL: localhost:3000/albums/all/show
```



## Surveys



### 1. Question



***Endpoint:***

```bash
Method: 
Type: 
URL: 
```



### 2. Answer



***Endpoint:***

```bash
Method: 
Type: 
URL: 
```



### 3. localhost:3000/sondages/


Post a sondage


***Endpoint:***

```bash
Method: POST
Type: RAW
URL: localhost:3000/sondages/
```



***Body:***

```js        
{
    "createdBy":"Nassim.bougtib@hotmail.fr",
    "event":"6274524b3bd01723c54957fe",
    "title":"My sondage",
    "question":"How do you...."

}
```



### 4. localhost:3000/sondages/idSondage/show


Show sondage information


***Endpoint:***

```bash
Method: GET
Type: 
URL: localhost:3000/sondages/627466a67af0fc929f6c3c90/show
```



### 5. localhost:3000/sondages/idSondage/update


Update a sondage


***Endpoint:***

```bash
Method: PUT
Type: RAW
URL: localhost:3000/sondages/627466a67af0fc929f6c3c90/update
```



***Body:***

```js        
{
    "title":"Sondage update"
}
```



### 6. localhost:3000/sondages/:idSondage/remove


Remove a sondage


***Endpoint:***

```bash
Method: DELETE
Type: 
URL: localhost:3000/sondages/627466a67af0fc929f6c3c90/remove
```



### 7. localhost:3000/sondages/:idSondage/questions/show



***Endpoint:***

```bash
Method: GET
Type: 
URL: localhost:3000/sondages/627466e97af0fc929f6c3caa/questions/show
```



### 8. localhost:3000/sondages/all/show



***Endpoint:***

```bash
Method: GET
Type: 
URL: localhost:3000/sondages/all/show
```



## Users

In this part you can create a new user, update the information of a user, remove user and all his creations (Comments, messages, pictures, ...) or remove him in the events and groups which is linked.



### 1. localhost:3000/users


## Create a User:

This endpoint use a POST method.

**You have to give in the body request the following informations :**

*   "firstname": the first name of the user,
*   "lastname": The last name of the event,
*   "email" : the email of user, be careful is unique.
*   "password": should be at least 6 characters,
*   "dob": The date of birth of the user in a Date format (YYYY-MM-D)
    

**You can also give optional parameter :**

*   "coverPicture": You have to give a string with valid image extension (Jpeg, jpg,png)
*   "profilePicture": You have to give a string with valid image extension (Jpeg, jpg,png)
    

After you send the request, you will see the details of the new **User** you just created

If the information is incorrectly entered, you will get an error message.


***Endpoint:***

```bash
Method: POST
Type: RAW
URL: localhost:3000/users
```



***Body:***

```js        
{
    "email":"Nassim.bougtib@hotmail.fr",
    "firstname":"Nassim",
    "lastname":"BOUGTIB",
    "dob":"2000-03-15",
    "password":"MyPassword1234",
    "coverPicture":"cover.png",
    "profilePicture":"profile.jpeg"
}
```



### 2. localhost:3000/users/all/show


## Display all Users:

You can display **all Users** of the collection with this endpoint.

After you send the request, you will see the **all the users which are stored inside the users collection in Mongodb** with their informations or an empty array if you don't have stored yet a user inside the collection.


***Endpoint:***

```bash
Method: GET
Type: 
URL: localhost:3000/users/all/show
```



### 3. localhost:3000/users/:id/show


## Show a User informations :

This endpoint use a GET method.

You have to give in the URL the object id of the specific **User.**

After you send the request, you will see the details of the specific User like his firstname, lastname, email, cover picture, ...

If the User does not exist then you will see the message **"User not found".**


***Endpoint:***

```bash
Method: GET
Type: 
URL: localhost:3000/users/62576f5b7a3a48d489993d99/show
```



### 4. localhost:3000/users/:id/update


## Update a User informations :

This endpoint use a PUT method.

You can update the information of specific user by giving is id object in url parameter.

You could give in the body any informations you want to update except the email. (Please ref to the post method to use the good fields).

After you send the request, you will see the new details of the user you just update.

If the information is incorrectly entered, you will get an error message.


***Endpoint:***

```bash
Method: PUT
Type: RAW
URL: localhost:3000/users/62576f5b7a3a48d489993d99/update
```



***Body:***

```js        
{
    "password":"NewPassword1234"
}
```



### 5. localhost:3000/users/:id/remove


## Delete a specific User:

You can remove a specific **User** with this endpoint by giving in the URL the object id of the **User**

When you delete a User, it would delete all his creations except the groups and events but he would be remove in the list of participants/members or managers/admins

After you send the request, your group would be deleted in the **users collection in mongoDB and you will see the message "User delete successfully"** or it could return a message **"User not found"** if you give a wrong object ID.


***Endpoint:***

```bash
Method: DELETE
Type: 
URL: localhost:3000/users/62576f5b7a3a48d489993d99/remove
```



---
[Back to top](#my-social-network)

>Generated at 2022-05-06 04:28:21 by [docgen](https://github.com/thedevsaddam/docgen)
