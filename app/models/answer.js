const mongoose = require('mongoose');
const GUID = require('mongoose-guid')(mongoose);
const uniqueValidator = require('mongoose-unique-validator');

const Schema = new mongoose.Schema({
    title: {
      type: String,
      required: [true, 'Title is required'],
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User',
        immutable: true
    },
    users: {
        type: [mongoose.Schema.Types.ObjectId],
        ref: 'User'
    },
    event:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Event',
        immutable: true
    },
    sondage:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Sondage',
        immutable: true
    },
    question:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Question',
        immutable: true
    }
}, {
    collection:'answers',
    // Bien optimiser la requête
    minimize: false,
    versionKey: false
}).set('toJSON',{
    transform: (doc, ret) => {
        // doc : Données qu'on récupère
        ret.id = ret._id;

        delete ret._id;
    }
});

Schema.plugin(uniqueValidator, { message: '{PATH} already exist' });

module.exports = Schema;