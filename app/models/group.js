const mongoose = require('mongoose');
const GUID = require('mongoose-guid')(mongoose);
const uniqueValidator = require('mongoose-unique-validator');
const UserModel = require('./user.js');

const Schema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Group name is required']
    },
    description: {
        type: String,
        required: [true, 'Group description is required']
    },
    icone: String,
    coverPicture: String,
    type: {
        type: String,
        enum:['Public','Private', 'Secret'],
        required: true
    },
    members:{ 
        type: [mongoose.Schema.Types.ObjectId], 
        ref: 'User',
        required: true,
        validate: v => Array.isArray(v) && v.length > 0
    },
    admins:{ 
        type: [mongoose.Schema.Types.ObjectId], 
        ref: 'User',
        required: true,
        validate: v => Array.isArray(v) && v.length > 0
    },
    createdBy: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User', 
        required: true 
    },
    events: {
        type: [mongoose.Schema.Types.ObjectId], 
        ref: 'Event'
    }
}, {
    collection:'groups',
    // Bien optimiser la requête
    minimize: false,
    versionKey: false
}).set('toJSON',{
    transform: (doc, ret) => {
        // doc : Données qu'on récupère
        ret.id = ret._id;

        delete ret._id;
    }
});

Schema.plugin(uniqueValidator, { message: '{PATH} already exist' });

module.exports = Schema;