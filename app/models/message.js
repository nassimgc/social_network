const mongoose = require('mongoose');
const GUID = require('mongoose-guid')(mongoose);
const uniqueValidator = require('mongoose-unique-validator');
const UserModel = require('./user.js');

const Schema = new mongoose.Schema({
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User',
        immutable: true
    },
    content: {
      type: String,
      required: [true, 'Content is required'],
    },
    comments: {
        type: [mongoose.Schema.Types.ObjectId],
        ref: 'Comment'
    },
    discussion:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Discussion',
        required: true,
        immutable: true
    },
    event:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Event',
        immutable: true
    },
    group:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Group',
        immutable: true
    }
}, {
    collection:'messages',
    // Bien optimiser la requête
    minimize: false,
    versionKey: false
}).set('toJSON',{
    transform: (doc, ret) => {
        // doc : Données qu'on récupère
        ret.id = ret._id;

        delete ret._id;
    }
});

Schema.plugin(uniqueValidator, { message: '{PATH} already exist' });

module.exports = Schema;