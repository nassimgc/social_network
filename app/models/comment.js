const mongoose = require('mongoose');
const GUID = require('mongoose-guid')(mongoose);
const uniqueValidator = require('mongoose-unique-validator');
const UserModel = require('./user.js');

const Schema = new mongoose.Schema({
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User',
        immutable: true
    },
    content: {
      type: String,
      required: [true, 'Content is required'],
    },
    type: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, 'Type is required'],
        refPath: 'onModel',
        immutable: true
    },
    onModel: {
      type: String,
      required: [true, 'Model is required'],
      enum: ['Message', 'Picture'],
      immutable: true
    },
    event:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Event',
        immutable: true
    },
    group:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Group',
        immutable: true
    },
    album:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Album',
        immutable: true
    },
    discussion:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Discussion',
        immutable: true
    },
    
}, {
    collection:'comments',
    // Bien optimiser la requête
    minimize: false,
    versionKey: false
}).set('toJSON',{
    transform: (doc, ret) => {
        // doc : Données qu'on récupère
        ret.id = ret._id;

        delete ret._id;
    }
});

Schema.plugin(uniqueValidator, { message: '{PATH} already exist' });

module.exports = Schema;