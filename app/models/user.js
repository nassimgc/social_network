const mongoose = require('mongoose');
const GUID = require('mongoose-guid')(mongoose);
const uniqueValidator = require('mongoose-unique-validator');

const Schema = new mongoose.Schema({
    firstname: {
        type: String,
        required: [true, 'first name is required']
    },
    lastname: {
        type: String,
        required: [true, 'last name is required']
    },
    email: {
        type: String,
        required: [true, 'Email is required'],
        unique: [true, 'Email already exist'],
        match: [/.+@.+\..+/],
        immutable: true,
    },
    password: {
        type: String,
        required: [true, 'Password is required'],
        minlength: [6, 'Password must be more than 6 characters']
    },
    dob: {
        type: Date,
        required: [true, 'Date of birth is required'],
    },
    coverPicture: {
        type: String,
        validate: {
            validator: function(v) {
                var re = /[\/.](jpg|jpeg|png)$/i;
                return (!v || !v.trim().length) || re.test(v)
            },
            message: 'cover picture format is invalid (jpg, jpeg, png).'
        },
    },
    profilePicture: {
        type: String,
        validate: {
            validator: function(v) {
                var re = /[\/.](jpg|jpeg|png)$/i;
                return (!v || !v.trim().length) || re.test(v)
            },
            message: 'cover picture format is invalid (jpg, jpeg, png).'
        },
    },
    token:{ 
        type: String, 
        default: GUID.value,
        immutable: true
    }
}, {
    collection:'users',
    // Bien optimiser la requête
    minimize: false,
    versionKey: false
}).set('toJSON',{
    transform: (doc, ret) => {
        // doc : Données qu'on récupère
        ret.id = ret._id;

        delete ret._id;
    }
});

Schema.plugin(uniqueValidator, { message: '{PATH} already exist' });

module.exports = Schema;