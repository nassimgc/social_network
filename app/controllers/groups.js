const UserModel = require('../models/user.js');
const GroupModel = require('../models/group.js');
const EventModel = require('../models/event.js');
const DiscussionModel = require('../models/discussion.js');
const MessageModel = require('../models/message.js');
const CommentModel = require('../models/comment.js');
const AlbumModel = require('../models/album.js');
const PictureModel = require('../models/picture.js');
const SondageModel = require('../models/sondage.js');
const QuestionModel = require('../models/question.js');
const AnswerModel = require('../models/answer.js');

module.exports = class Groups {
    constructor(app, connect) {
        this.app = app;
        this.UserModel = connect.model('User', UserModel);
        this.GroupModel = connect.model('Group', GroupModel);
        this.EventModel = connect.model('Event', EventModel);
        this.DiscussionModel = connect.model('Discussion', DiscussionModel);
        this.MessageModel = connect.model('Message', MessageModel);
        this.CommentModel = connect.model('Comment', CommentModel);
        this.AlbumModel = connect.model('Album', AlbumModel);
        this.PictureModel = connect.model('Picture', PictureModel);
        this.SondageModel = connect.model('Sondage', SondageModel);
        this.QuestionModel = connect.model('Question', QuestionModel);
        this.AnswerModel = connect.model('Answer', AnswerModel);
        this.run();
    }

    showAllGroups(){
        this.app.get('/groups/:id/show',async (req, res)=> {
            try{
                this.GroupModel.find({})
                .populate({
                    path: 'admins',
                    select: 'email'
                })
                .populate({
                    path: 'createdBy',
                    select: 'email'
                })
                .populate({
                    path: 'members',
                    select: 'email'
                })
                .populate({
                    path: 'events',
                    populate: [
                        { 
                        path: 'createdBy', 
                        select:'email' 
                        },
                        { 
                        path: 'managers', 
                        select:'email' 
                        },
                        { 
                        path: 'participants', 
                        select:'email' 
                        }
                    ]
                })
                .then((groups) => {
                    res.status(200).json(groups || 'Groups not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });
            } catch(err) {
                console.error(`[ERROR] get:groups -> ${err}`);
            
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    createGroup(){
        this.app.post('/groups/',async (req, res)=> {
            try{
                const userQuery = this.UserModel.findOne({ email: req.body.createdBy }).then((user) => {
                    return user
                }).catch((err)=>{  
                    res.status(422).json(`Search user step : ${err.message}` || {});
                });

                const user = await userQuery;
                if(!user){
                    res.status(404).json('User not found');
                    return;
                }
                req.body.createdBy = user._id;
                if(!req.body.admins){
                    req.body.admins = [];
                }
                if(!req.body.members){
                    req.body.members = [];
                }
                req.body.admins.push(user._id);
                req.body.members.push(user._id);
                const groupModelQuery = new this.GroupModel(req.body);
                let errors = groupModelQuery.validateSync();
                if(errors){
                    // throw new Error(errors);
                    res.status(422).json(`Error validation group step : ${errors.message}` || {});
                    return;
                }
                groupModelQuery.save().then((group) => {
                    res.status(200).json(group || {});
                }).catch((err)=>{  
                    res.status(422).json(`Save group step : ${err.message}` || {});
                });

            } catch(err) {
                console.error(`[ERROR] post:group -> ${err}`);
            
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    showGroup(){
        this.app.get('/groups/:id/show',async (req, res)=> {
            try{
                this.GroupModel.findOne({ _id: req.params.id })
                .populate({
                    path: 'admins',
                    select: 'email'
                })
                .populate({
                    path: 'createdBy',
                    select: 'email'
                })
                .populate({
                    path: 'members',
                    select: 'email'
                })
                .populate({
                    path: 'events',
                    populate: [
                        { 
                        path: 'createdBy', 
                        select:'email' 
                        },
                        { 
                        path: 'managers', 
                        select:'email' 
                        },
                        { 
                        path: 'participants', 
                        select:'email' 
                        }
                    ]
                })
                .then((group) => {
                    res.status(200).json(group || 'Group not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });
            } catch(err) {
                console.error(`[ERROR] get:group -> ${err}`);
            
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    updateGroup(){
        this.app.put('/groups/:id/update', async (req, res) => {
            try {
                    this.GroupModel.findOneAndUpdate({_id: req.params.id}, 
                        req.body, {
                        // On renvoit le group avec les nouvelles valeurs
                        new: true,
                        // Applique les règles de validation du modèle (Group)
                        runValidators: true
                    }).then((group) => {
                            res.status(200).json(group || 'Group not found');
                    }).catch((err)=>{  
                            res.status(422).json(err.message || {});
                    });
            }
            catch (err) {
                console.error(`[ERROR] put:group -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    deleteGroup(){
        this.app.delete('/groups/:id/delete', async (req, res) => {
            try {
                const groupQuery = this.GroupModel.findOne({ _id: req.params.id })
                .then((group) => {
                    return group;
                }).catch((err)=>{  
                    res.status(422).json(`Search group step : ${err.message}`  || {});
                });

                const group = await groupQuery;

                if(!group){
                    res.status(404).json('Group not found');
                    return;
                }else{
                    if(group.events.length > 0){
                        group.events.forEach(event => {
                            this.DiscussionModel.deleteMany({ type: event._id }).catch((err)=>{
                                res.status(422).json(`Remove comments of events: ${err.message}`  || {});
                                return;
                            }); 
                            this.AlbumModel.deleteMany({ event: event._id  }).catch((err)=>{
                                res.status(422).json(`Remove albums of event: ${err.message}`  || {});
                                return;
                            });
                            this.MessageModel.deleteMany({ event: event._id  }).catch((err)=>{
                                res.status(422).json(`Remove messages of event: ${err.message}`  || {});
                                return;
                            });
                            this.PictureModel.deleteMany({ event: event._id  }).catch((err)=>{
                                res.status(422).json(`Remove pictures of event: ${err.message}`  || {});
                                return;
                            });

                            this.CommentModel.deleteMany({ event: event._id }).catch((err)=>{
                                res.status(422).json(`Remove comments of event: ${err.message}`  || {});
                            }); 

                            this.AnswerModel.deleteMany({ event: event._id }).catch((err)=>{
                                res.status(422).json(`Remove answers of event: ${err.message}`  || {});
                            }); 
        
                            this.QuestionModel.deleteMany({ event: event._id }).catch((err)=>{
                                res.status(422).json(`Remove questions of event: ${err.message}`  || {});
                            }); 
        
                            this.SondageModel.deleteMany({ event: event._id }).catch((err)=>{
                                res.status(422).json(`Remove sondages of event: ${err.message}`  || {});
                            }); 
        
                        });
                    }
                }
                this.DiscussionModel.deleteMany({ type: group._id }).catch((err)=>{
                    res.status(422).json(`Remove comments of group: ${err.message}`  || {});
                    return;
                }); 

                this.MessageModel.deleteMany({ group: group._id  }).catch((err)=>{
                    res.status(422).json(`Remove messages of group: ${err.message}`  || {});
                    return;
                });

                this.CommentModel.deleteMany({ group: group._id }).catch((err)=>{
                    res.status(422).json(`Remove comments of group: ${err.message}`  || {});
                }); 

                this.EventModel.deleteMany({ group: group._id }).catch((err)=>{
                    res.status(422).json(`Remove comments of group: ${err.message}`  || {});
                }); 
                this.GroupModel.deleteOne({_id: group._id}).then((d)=>{
                    if (d.acknowledged && d.deletedCount == 1){
                        res.status(200).json('Group successfully delete');
                    }else{
                        res.status(200).json('Couldn\'t delete group');
                    }
                }).catch((err)=>{
                    res.status(422).json(`Remove group: ${err.message}`  || {});
                });
            }
            catch (err) {
                console.error(`[ERROR] delete:group -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    addMember(){
        this.app.put('/groups/:id/member/add', async (req, res) => {
            try {
                const userQuery = this.UserModel.findOne({ email: req.body.member }).then((user) => {
                    return user;
                }).catch((err)=>{  
                    res.status(422).json(`Search user step : ${err.message}`  || {});
                });

                const user = await userQuery;

                if(!user){
                    res.status(404).json('User not found');
                    return;
                }
                this.GroupModel.findOneAndUpdate({_id: req.params.id}, 
                    { $addToSet: { members: user._id } }, {
                    // On renvoit le group avec les nouvelles valeurs
                    new: true,
                    // Applique les règles de validation du modèle (Group)
                    runValidators: true
                }).then((group) => {
                        res.status(200).json(group || 'Group not found');
                }).catch((err)=>{  
                    res.status(422).json(`Add member step : ${err.message}` || {});
                });
            }
            catch (err) {
                console.error(`[ERROR] put: add member -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    showMembers(){
        this.app.get('/groups/:id/member/show', async (req, res) => {
            try {
                this.GroupModel.findOne({ _id: req.params.id }, {members: 1, _id: 0})
                .populate({
                    path: 'members',
                    select: 'email'
                  })
                .then((group) => {
                    res.status(200).json(group || 'Group not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

            }
            catch (err) {
                console.error(`[ERROR] get:members -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    removeMember(){
        this.app.delete('/groups/:id/member/remove', async (req, res) => {
            try {
                const userQuery = this.UserModel.findOne({ email: req.body.member }).then((user) => {
                    return user;
                }).catch((err)=>{  
                    res.status(422).json(`Search user step : ${err.message}`  || {});
                });

                const user = await userQuery;
                if(!user){
                    res.status(404).json('User not found');
                    return;
                }
                this.GroupModel.findOneAndUpdate({_id: req.params.id}, 
                    {   
                        $pull: {
                        members: user._id,
                        }
                    }, 
                    {
                        // On renvoit l'utilisateur avec les nouvelles valeurs
                        new: true,
                        // Applique les règles de validation du modèle (User)
                        runValidators: true
                    }).then((group) => {
                            res.status(200).json(group || 'Group not found');
                    }).catch((err)=>{  
                        res.status(422).json(`Remove member step : ${err.message}`  || {});
                    });
            }
            catch (err) {
                console.error(`[ERROR] delete: remove member -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    addAdmin(){
        this.app.put('/groups/:id/admin/add', async (req, res) => {
            try {
                const userQuery = this.UserModel.findOne({ email: req.body.admin }).then((user) => {
                    return user;
                }).catch((err)=>{  
                    res.status(422).json(`Search user step : ${err.message}`  || {});
                });

                const user = await userQuery;
                if(!user){
                    res.status(404).json('User not found');
                    return;
                }
                this.GroupModel.findOneAndUpdate({_id: req.params.id}, 
                    { $addToSet: { admins: user._id } }, {
                    // On renvoit l'utilisateur avec les nouvelles valeurs
                    new: true,
                    // Applique les règles de validation du modèle (User)
                    runValidators: true
                }).then((group) => {
                        res.status(200).json(group || 'Group not found');
                }).catch((err)=>{  
                    res.status(422).json(`Add admin step : ${err.message}`  || {});
                });
            }
            catch (err) {
                console.error(`[ERROR] put:add admin -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    showAdmins(){
        this.app.get('/groups/:id/admin/show', async (req, res) => {
            try {
                this.GroupModel.findOne({ _id: req.params.id }, {admins: 1, _id: 0})
                .populate({
                    path: 'admins',
                    select: 'email'
                  })
                .then((group) => {
                    res.status(200).json(group || 'Group not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

            }
            catch (err) {
                console.error(`[ERROR] get:admins -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    removeAdmin(){
        this.app.delete('/groups/:id/admin/remove', async (req, res) => {
            try {
                const userQuery = this.UserModel.findOne({ email: req.body.admin }).then((user) => {
                    return user;
                }).catch((err)=>{  
                    res.status(422).json(`Search user step : ${err.message}`  || {});
                });

                const user = await userQuery;
                if(!user){
                    res.status(404).json('User not found');
                    return;
                }
                this.GroupModel.findOneAndUpdate({_id: req.params.id}, 
                    {   
                        $pull: {
                        admins: user._id,
                        }
                    }, 
                    {
                        // On renvoit le group avec les nouvelles valeurs
                        new: true,
                        // Applique les règles de validation du modèle (Group)
                        runValidators: true
                    }).then((group) => {
                            res.status(200).json(group || 'Group not found');
                    }).catch((err)=>{  
                        res.status(422).json(`Remove admin step : ${err.message}`  || {});
                    });
            }
            catch (err) {
                console.error(`[ERROR] delete: remove admin -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    addEvent(){
        this.app.put('/groups/:id/event/add', async (req, res) => {
            try {
                const eventQuery = this.EventModel.findOne({ _id: req.body.event }).then((event) => {
                    return event;
                }).catch((err)=>{  
                    res.status(422).json(`Search event step : ${err.message}`  || {});
                });

                const event = await eventQuery;
                if(!event){
                    res.status(404).json('Event not found');
                    return;
                }
                this.GroupModel.findOneAndUpdate({_id: req.params.id}, 
                    { $addToSet: { events: event._id } }, {
                    // On renvoit l'utilisateur avec les nouvelles valeurs
                    new: true,
                    // Applique les règles de validation du modèle (User)
                    runValidators: true
                }).then((group) => {
                        res.status(200).json(group || 'Group not found');
                }).catch((err)=>{  
                    res.status(422).json(`Add event step : ${err.message}`  || {});
                });
            }
            catch (err) {
                console.error(`[ERROR] put:add event -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    showEvents(){
        this.app.get('/groups/:id/event/show', async (req, res) => {
            try {
                this.GroupModel.findOne({ _id: req.params.id }, {events: 1, _id: 0})
                .populate({
                    path: 'events',
                    populate:[
                        { 
                            path: 'createdBy', 
                            select:'email -_id' 
                        },
                        { 
                            path: 'managers', 
                            select:'email -_id' 
                        },
                        { 
                            path: 'participants', 
                            select:'email -_id' 
                        }
                    ]
                  })
                .then((group) => {
                    res.status(200).json(group || 'Group not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

            }
            catch (err) {
                console.error(`[ERROR] get:events -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    removeEvent(){
        this.app.delete('/groups/:id/event/remove', async (req, res) => {
            try {
                const eventQuery = this.EventModel.findOne({ _id: req.body.event }).then((event) => {
                    return event;
                }).catch((err)=>{  
                    res.status(422).json(`Search user step : ${err.message}`  || {});
                });

                const event = await eventQuery;
                if(!event){
                    res.status(404).json('Event not found');
                    return;
                }
                this.GroupModel.findOneAndUpdate({_id: req.params.id}, 
                    {   
                        $pull: {
                        events: event._id,
                        }
                    }, 
                    {
                        // On renvoit le group avec les nouvelles valeurs
                        new: true,
                        // Applique les règles de validation du modèle (Group)
                        runValidators: true
                    }).then((group) => {
                            res.status(200).json(group || 'Group not found');
                    }).catch((err)=>{  
                        res.status(422).json(`Remove event step : ${err.message}`  || {});
                    });
            }
            catch (err) {
                console.error(`[ERROR] delete: remove event -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }
    run() {
        // Group
        this.createGroup();
        this.showAllGroups();
        this.updateGroup();
        this.showGroup();
        this.deleteGroup();
        // Members
        this.addMember();
        this.showMembers();
        this.removeMember();
        // Admins
        this.addAdmin();
        this.showAdmins();
        this.removeAdmin();
        // Events
        this.addEvent();
        this.showEvents();
        this.removeEvent();
    }}