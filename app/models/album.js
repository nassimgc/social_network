const mongoose = require('mongoose');
const GUID = require('mongoose-guid')(mongoose);
const uniqueValidator = require('mongoose-unique-validator');
const UserModel = require('./user.js');

const Schema = new mongoose.Schema({
    event: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, 'Event is required'],
        ref: 'Event',
        immutable: true
    },
    pictures: {
        type:[mongoose.Schema.Types.ObjectId],
        ref:'Picture'
    },
    createdBy: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User', 
        required: true,
        immutable: true
     }
}, {
    collection:'albums',
    // Bien optimiser la requête
    minimize: false,
    versionKey: false
}).set('toJSON',{
    transform: (doc, ret) => {
        // doc : Données qu'on récupère
        ret.id = ret._id;

        delete ret._id;
    }
});

Schema.plugin(uniqueValidator, { message: '{PATH} already exist' });

module.exports = Schema;