const EventModel = require('../models/event.js');
const UserModel = require('../models/user.js');
const GroupModel = require('../models/group.js');
const DiscussionModel = require('../models/discussion.js');
const MessageModel = require('../models/message.js');
const CommentModel = require('../models/comment.js');


module.exports = class Discussions {
    constructor(app, connect) {
        this.app = app;
        this.EventModel = connect.model('Event', EventModel);
        this.UserModel = connect.model('User', UserModel);
        this.GroupModel = connect.model('Group', GroupModel);
        this.DiscussionModel = connect.model('Discussion', DiscussionModel);
        this.MessageModel = connect.model('Message', MessageModel);
        this.CommentModel = connect.model('Comment', CommentModel);
        
        this.run();
    }

    showAllDiscussions(){
        this.app.get('/discussions/all/show',async (req, res)=> {
            try{
                this.DiscussionModel.find({}).populate({
                    path: 'type',
                    populate: [
                        { 
                        path: 'createdBy', 
                        select:'email -_id' 
                        },
                        { 
                        path: 'managers', 
                        select:'email -_id',
                        strictPopulate: false 
                        },
                        { 
                        path: 'participants', 
                        select:'email -_id',
                        strictPopulate: false
                        },
                        { 
                        path: 'admins', 
                        select:'email -_id',
                        strictPopulate: false
                        },
                        { 
                        path: 'members', 
                        select:'email -_id',
                        strictPopulate: false
                        },
                    ]
                })
                .populate({
                    path: 'createdBy',
                    select: 'email -_id'
                })
                .populate({
                    path: 'messages',
                    populate: [
                        { 
                        path: 'createdBy', 
                        select:'email -_id' 
                        },
                        { 
                            path: 'comments', 
                            select:'content -_id' 
                        },
                    ],
                    select: ' -discussion'
                  })
                .then((discu) => {
                    res.status(200).json(discu || 'Discussion not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });
            } catch(err) {
                console.error(`[ERROR] get:discussion -> ${err}`);
            
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    createDiscussion() {
        this.app.post('/discussions/',async (req, res)=> {
            try{

                const userQuery = this.UserModel.findOne({ email: req.body.createdBy }).then((user) => {
                    return user;
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                    return;
                });

                const user = await userQuery;
                if(!user){
                    res.status(404).json('User not found');
                    return;
                }
                req.body.createdBy = user._id;
                let typeQuery = null;
                if(req.body.onModel == 'Event'){
                    typeQuery = this.EventModel.findOne({_id: req.body.type }).then((evenment)=>{
                            return evenment;
                    }).catch((err)=>{  
                        res.status(422).json(err.message || {});
                    });
                }else{
                    typeQuery = this.GroupModel.findOne({_id: req.body.type }).then((group)=>{
                        return group;
                    }).catch((err)=>{  
                        res.status(422).json(err.message || {});
                    });
                }
                const type = await typeQuery;
                if(!type){
                    res.status(404).json(`${req.body.onModel} not found`);
                    return;
                }

                if(req.body.onModel == 'Event'){
                    if(!type.managers.includes(user._id) || !type.participants.includes(user._id)){
                        res.status(403).json('You\'re not allowed create discussion inside this event');
                        return;
                    }
                }else{
                    if(!type.members.includes(user._id) || !type.admins.includes(user._id)){
                        res.status(403).json('You\'re not allowed create discussion inside this group');
                        return;
                    }
                }
                let body = {
                    'type': req.body.type,
                    'onModel': req.body.onModel,
                    'createdBy': req.body.createdBy
                };
                const discussionModelQuery = new this.DiscussionModel(body);
                discussionModelQuery.save().then((discu) => {
                    res.status(200).json(discu || {});
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

            } catch(err) {
                console.error(`[ERROR] post:discussions -> ${err}`);
            
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    showDiscussion(){
        this.app.get('/discussions/:id/show',async (req, res)=> {
            try{
                this.DiscussionModel.findOne({ _id: req.params.id }).populate({
                    path: 'type',
                    populate: [
                        { 
                        path: 'createdBy', 
                        select:'email -_id' 
                        },
                        { 
                        path: 'managers', 
                        select:'email -_id',
                        strictPopulate: false 
                        },
                        { 
                        path: 'participants', 
                        select:'email -_id',
                        strictPopulate: false
                        },
                        { 
                        path: 'admins', 
                        select:'email -_id',
                        strictPopulate: false
                        },
                        { 
                        path: 'members', 
                        select:'email -_id',
                        strictPopulate: false
                        },
                    ]
                })
                .populate({
                    path: 'createdBy',
                    select: 'email -_id'
                })
                .populate({
                    path: 'messages',
                    populate: [
                        { 
                        path: 'createdBy', 
                        select:'email -_id' 
                        },
                        { 
                            path: 'comments', 
                            select:'content -_id' 
                        },
                    ],
                    select: ' -discussion'
                  })
                .then((discu) => {
                    res.status(200).json(discu || 'Discussion not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });
            } catch(err) {
                console.error(`[ERROR] get:discussion -> ${err}`);
            
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    updateDiscussion(){
        this.app.put('/discussions/:id/update', (req, res) => {
            try {
                    this.DiscussionModel.findOneAndUpdate({_id: req.params.id}, 
                        req.body, {
                        // On renvoit la discussion avec les nouvelles valeurs
                        new: true,
                        // Applique les règles de validation du modèle (Discussion)
                        runValidators: true
                    }).then((discu) => {
                            res.status(200).json(discu || 'Discussion not found');
                    }).catch((err)=>{  
                            res.status(422).json(err.message || {});
                    });
            }
            catch (err) {
                console.error(`[ERROR] put:discussion -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    deleteDiscussion(){
        this.app.delete('/discussions/:id/remove', async (req, res) => {
            try {
                const discussionQuery = this.DiscussionModel.findOne({ _id: req.params.id })
                .then((discussion) => {
                    return discussion;
                }).catch((err)=>{  
                    res.status(422).json(`Search discussion step : ${err.message}`  || {});
                });

                const discussion = await discussionQuery;

                if(!discussion){
                    res.status(404).json('Discussion not found');
                    return;
                }

                // const messageQuery = this.MessageModel.updateMany({ discussion: discussion._id })
                // .then((messages) => {
                //     return messages;
                // }).catch((err)=>{  
                //     res.status(422).json(`Search message step : ${err.message}`  || {});
                // });
                
                // // On récupère la liste des messages pour d'abord supprimer les commentaires liés
                // let messages = await messageQuery;

                // if(messages.length > 0){
                //     messages.forEach(message => {
                //         this.CommentModel.deleteMany({ type: message._id, onModel: "Message" }).catch((err)=>{
                //             res.status(422).json(`Remove comments of message: ${err.message}`  || {});
                //         });                   
                //     });
                // }

                this.CommentModel.deleteMany({ discussion: discussion._id, onModel: "Message" }).catch((err)=>{
                    res.status(422).json(`Remove comments of discussion: ${err.message}`  || {});
                }); 
                this.MessageModel.deleteMany({ discussion: discussion._id  }).catch((err)=>{
                    res.status(422).json(`Remove messages of discussion: ${err.message}`  || {});
                });

                this.DiscussionModel.deleteOne({_id: discussion._id}).then((d)=>{
                    if (d.acknowledged && d.deletedCount == 1){
                        res.status(200).json('Discussion successfully delete');
                    }else{
                        res.status(200).json('Couldn\'t delete discussion');
                    }
                }).catch((err)=>{
                    res.status(422).json(`Remove discussion: ${err.message}`  || {});
                });
            }
            catch (err) {
                console.error(`[ERROR] delete:discussion -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    addMessage(){
        this.app.post('/discussions/:id/message/add', async (req, res) => {
            try {
                const discussionQuery = this.DiscussionModel.findOne({ _id: req.params.id }).then((discussion) => {
                    return discussion;
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

                const discussion = await discussionQuery;
                if(!discussion){
                    res.status(404).json('Discussion not found');
                    return;
                }

                const userQuery = this.UserModel.findOne({ email: req.body.createdBy }).then((user) => {
                    return user;
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

                const user = await userQuery;
                if(!user){
                    res.status(404).json('User not found');
                    return;
                }

                if(discussion.onModel == 'Event'){

                    let getEventQuery = this.EventModel.findOne({ _id: discussion.type }).then((event) => {
                        return event;
                    }).catch((err)=>{  
                        res.status(422).json(err.message || {});
                        return;
                    });

                    let event = await getEventQuery;
                    if(!event.managers.includes(user._id) || !event.participants.includes(user._id)){
                        res.status(403).json('You\'re not allowed create message inside this discussion');
                        return;
                    }

                }else{

                    let getGroupQuery = this.GroupModel.findOne({ _id: discussion.type }).then((group) => {
                        return group;
                    }).catch((err)=>{  
                        res.status(422).json(err.message || {});
                        return;
                    });

                    let group = await getGroupQuery;
                    if(!group.members.includes(user._id) || !group.admins.includes(user._id)){
                        res.status(403).json('You\'re not allowed create message inside this discussion');
                        return;
                    }
                }

                let body = {
                    'createdBy': user._id,
                    'discussion': discussion._id,
                    'content': req.body.content
                };
                if(discussion.onModel == 'Event'){
                    body.event = discussion._id;
                }else{
                    body.group = discussion._id;
                }
                const messageModelQuery = new this.MessageModel(body);
                const messageQuerySave = messageModelQuery.save().then((message) => {
                    return message;
                }).catch((err)=>{  
                    return(err.message || {});
                });

                const message = await messageQuerySave;
                if(!message._id){
                    res.status(422).json(message);
                    return;
                }

                this.DiscussionModel.findOneAndUpdate({_id: req.params.id}, 
                    {$push: { messages: message  }}, {
                    // On renvoit l'utilisateur avec les nouvelles valeurs
                    new: true,
                    // Applique les règles de validation du modèle (User)
                    runValidators: true
                }).then((discu) => {
                        res.status(200).json(message || {});
                        return;
                }).catch((err)=>{  
                        res.status(422).json(err.message || {});
                        return;
                });

            }
            catch (err) {
                console.error(`[ERROR] post:message -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    showMessages(){
        this.app.get('/discussions/:id/messages/show', async (req, res) => {
            try {
                this.DiscussionModel.findOne({ _id: req.params.id }, {messages: 1, _id: 0})
                .populate({
                    path: 'messages',
                    populate:{
                        path: 'createdBy',
                        select:'email'
                    },
                    select: 'content'
                    // select: 'content -_id'
                  })
                .then((discussion) => {
                    res.status(200).json(discussion || 'Discussion not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

            }
            catch (err) {
                console.error(`[ERROR] get:messages -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    updateMessage(){
        this.app.put('/message/:id/update', (req, res) => {
            try {
                    // console.log(typeof req.body.managers);
                    this.MessageModel.findOneAndUpdate({_id: req.params.id}, 
                        req.body, {
                        // On renvoit le message avec les nouvelles valeurs
                        new: true,
                        // Applique les règles de validation du modèle (Message)
                        runValidators: true
                    }).then((message) => {
                            res.status(200).json(message || 'Message not found');
                    }).catch((err)=>{  
                            res.status(422).json(err.message || {});
                    });
            }
            catch (err) {
                console.error(`[ERROR] put:message -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    deleteMessage(){
        this.app.delete('/discussions/:idDiscussion/message/:idMessage/remove', async (req, res) => {
            try {
                const messageQuery = this.MessageModel.findOne({ _id: req.params.idMessage })
                .then((message) => {
                    return message;
                }).catch((err)=>{  
                    res.status(422).json(`Search message step : ${err.message}`  || {});
                });

                const message = await messageQuery;
                if(!message){
                    res.status(404).json('Message not found');
                    return;
                }

                const discussionQuery = this.DiscussionModel.findOneAndUpdate({_id: req.params.idDiscussion }, 
                    {   
                        $pull: {
                        messages: message._id,
                        }
                    }, 
                    {
                        // On renvoit le message avec les nouvelles valeurs
                        new: true,
                        // Applique les règles de validation du modèle (Message)
                        runValidators: true
                    }).then((discussion) => {
                            return discussion;
                    }).catch((err)=>{  
                        res.status(422).json(`Remove message in discussion step : ${err.message}`  || {});
                    });

                    const discussion = await discussionQuery;

                    if(!discussion){
                        res.status(404).json('Discussion not found');
                        return;
                    }
                    this.CommentModel.deleteMany({ type: message._id, onModel: "Message" }).catch((err)=>{
                        res.status(422).json(`Remove comments of message: ${err.message}`  || {});
                    });

                    this.MessageModel.deleteOne({_id: message._id}).then((d)=>{
                        if (d.acknowledged && d.deletedCount == 1){
                            res.status(200).json('Message successfully delete');
                        }else{
                            res.status(200).json('Couldn\'t delete message');
                        }
                    }).catch((err)=>{
                        res.status(422).json(`Remove message: ${err.message}`  || {});
                    });
            }
            catch (err) {
                console.error(`[ERROR] delete:message -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }


    addComment(){
        this.app.post('/discussions/:idDiscussion/message/:idMessage/comment/add', async (req, res) => {
            try {
                const discussionQuery = this.DiscussionModel.findOne({ _id: req.params.idDiscussion }).then((discussion) => {
                    return discussion;
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

                const discussion = await discussionQuery;
                if(!discussion){
                    res.status(404).json('Discussion not found');
                    return;
                }

                const messageQuery = this.MessageModel.findOne({ _id: req.params.idMessage }).then((message) => {
                    return message;
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

                const message = await messageQuery;
                if(!message){
                    res.status(404).json('Message not found');
                    return;
                }

                const userQuery = this.UserModel.findOne({ email: req.body.createdBy }).then((user) => {
                    return user;
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

                const user = await userQuery;
                if(!user){
                    res.status(404).json('User not found');
                    return;
                }

                if(req.body.onModel != 'Message'){
                    res.status(422).json('Model of comment should be Message');
                    return;
                }

                if(discussion.onModel == 'Event'){
                    let getEventQuery = this.EventModel.findOne({ _id: discussion.type }).then((event) => {
                        return event;
                    }).catch((err)=>{  
                        res.status(422).json(err.message || {});
                        return;
                    });
                    let event = await getEventQuery;
                    if(!event.managers.includes(user._id) || !event.participants.includes(user._id)){
                        res.status(403).json('You\'re not allowed to comment message inside this discussion');
                        return;
                    }
                }else{
                    let getGroupQuery = this.GroupModel.findOne({ _id: discussion.type }).then((group) => {
                        return group;
                    }).catch((err)=>{  
                        res.status(422).json(err.message || {});
                        return;
                    });
                    let group = await getGroupQuery;
                    if(!group.members.includes(user._id) || !group.admins.includes(user._id)){
                        res.status(403).json('You\'re not allowed to comment message inside this discussion');
                        return;
                    }
                }

                let body = {
                    'createdBy': user._id,
                    'content': req.body.content,
                    'type': message._id,
                    'onModel': req.body.onModel,
                    'discussion':discussion._id,

                };

                if(discussion.onModel == 'Event'){
                    body.event = discussion.type;
                }else{
                    body.group = discussion.type;
                }
                
                const commentModelQuery = new this.CommentModel(body);
                const commentQuerySave = commentModelQuery.save().then((comment) => {
                    return comment;
                }).catch((err)=>{  
                    // res.status(422).json(err.message || {});
                    return(err.message || {});
                });

                const comment = await commentQuerySave;
                if(!comment._id){
                    res.status(422).json(comment);
                    return;
                }

                this.MessageModel.findOneAndUpdate({_id: req.params.idMessage}, 
                    {$push: { comments: comment  }}, {
                    // On renvoit l'utilisateur avec les nouvelles valeurs
                    new: true,
                    // Applique les règles de validation du modèle (User)
                    runValidators: true
                }).then(() => {
                        res.status(200).json(comment || {});
                }).catch((err)=>{  
                        res.status(422).json(err.message || {});
                });
            }
            catch (err) {
                console.error(`[ERROR] post:comment -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    showCommentMessage(){
        this.app.get('/discussions/:idDiscussion/message/:idMessage/comments', async (req, res) => {
            try {
                this.DiscussionModel.findOne({ _id: req.params.idDiscussion }, {onModel: 0, type: 0, createdBy: 0, _id: 0, messages: {$elemMatch: {$eq: req.params.idMessage}}})
                .populate({
                    path: 'messages',
                    populate: [
                        { 
                            path: 'comments',
                            populate:{
                                path:'createdBy',
                                select:'email -_id'
                            },
                            select:'content createdBy _id' 
                            // select:'content createdBy -_id' 
                        },
                    ],
                    select: 'content -_id'
                  })
                .then((discussion) => {
                    res.status(200).json(discussion || 'Discussion or message not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

            }
            catch (err) {
                console.error(`[ERROR] get:messages -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    updateComment(){
        this.app.put('/comment/:id/update', (req, res) => {
            try {
                    // console.log(typeof req.body.managers);
                    this.CommentModel.findOneAndUpdate({_id: req.params.id}, 
                        req.body, {
                        // On renvoit l'utilisateur avec les nouvelles valeurs
                        new: true,
                        // Applique les règles de validation du modèle (User)
                        runValidators: true
                    }).then((comment) => {
                            res.status(200).json(comment || 'Comment not found');
                    }).catch((err)=>{  
                            res.status(422).json(err.message || {});
                    });
            }
            catch (err) {
                console.error(`[ERROR] put:comment -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    deleteComment(){
        this.app.delete('/discussions/:idDiscussion/message/:idMessage/comment/:idComment/remove', async (req, res) => {
            try {
                const commentQuery = this.CommentModel.findOne({ _id: req.params.idComment })
                .then((comment) => {
                    return comment;
                }).catch((err)=>{  
                    res.status(422).json(`Search comment step : ${err.message}`  || {});
                });
                const comment = await commentQuery;
                if(!comment){
                    res.status(404).json('Comment not found');
                    return;
                }
                const messageQuery = this.MessageModel.findOneAndUpdate({_id: req.params.idMessage, discussion: req.params.idDiscussion }, 
                    {   
                        $pull: {
                        comments: comment._id,
                        }
                    }, 
                    {
                        // On renvoit le message avec les nouvelles valeurs
                        new: true,
                        // Applique les règles de validation du modèle (Message)
                        runValidators: true
                    }).then((message) => {
                            return message;
                    }).catch((err)=>{  
                        res.status(422).json(`Remove comment in message step : ${err.message}`  || {});
                    });

                const message = await messageQuery;
                if(!message){
                    res.status(404).json('Message not found');
                    return;
                }

                this.CommentModel.deleteOne({_id: comment._id}).then((d)=>{
                    if (d.acknowledged && d.deletedCount == 1){
                        res.status(200).json('Comment successfully delete');
                    }else{
                        res.status(200).json('Couldn\'t delete message');
                    }
                }).catch((err)=>{
                    res.status(422).json(`Remove message: ${err.message}`  || {});
                });
            }
            catch (err) {
                console.error(`[ERROR] delete:comment -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    run() {
        this.createDiscussion();
        this.showAllDiscussions();
        this.showDiscussion();
        this.addMessage();
        this.showMessages();
        this.addComment();
        this.showCommentMessage();
        this.updateDiscussion();
        this.updateMessage();
        this.updateComment();
        this.deleteComment();
        this.deleteMessage();
        this.deleteDiscussion();
    }
}