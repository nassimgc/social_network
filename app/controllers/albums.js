const EventModel = require('../models/event.js');
const UserModel = require('../models/user.js');
const AlbumModel = require('../models/album.js');
const PictureModel = require('../models/picture.js');
const CommentModel = require('../models/comment.js');


module.exports = class Albums {
    constructor(app, connect) {
        this.app = app;
        this.EventModel = connect.model('Event', EventModel);
        this.UserModel = connect.model('User', UserModel);
        this.AlbumModel = connect.model('Album', AlbumModel);
        this.PictureModel = connect.model('Picture', PictureModel);
        this.CommentModel = connect.model('Comment', CommentModel);
        
        this.run();
    }

    showAllAlbums(){
        this.app.get('/albums/all/show',async (req, res)=> {
            try{
                this.AlbumModel.find({}).populate({
                    path: 'event',
                    populate: [
                        { 
                        path: 'createdBy', 
                        select:'email -_id' 
                        },
                        { 
                        path: 'managers', 
                        select:'email -_id' 
                        },
                        { 
                        path: 'participants', 
                        select:'email -_id' 
                        }
                    ]
                })
                .populate({
                    path: 'createdBy',
                    select: 'email -_id'
                })
                .populate({
                    path: 'pictures',
                    populate: [
                        { 
                        path: 'createdBy', 
                        select:'email -_id' 
                        },
                        { 
                            path: 'comments', 
                            select:'content -_id' 
                        },
                    ],
                    select: '-album'
                  })
                .then((album) => {
                    res.status(200).json(album || 'Album not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });
            } catch(err) {
                console.error(`[ERROR] get:album -> ${err}`);
            
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }
    createAlbum() {
        this.app.post('/albums/',async (req, res)=> {
            try{

                const userQuery = this.UserModel.findOne({ email: req.body.createdBy }).then((user) => {
                    return user;
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

                const user = await userQuery;
                if(!user){
                    res.status(404).json('User not found');
                    return;
                }
                const eventQuery = this.EventModel.findOne({_id: req.body.event }).then((evenment)=>{
                        return evenment;
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });
                const event = await eventQuery;
                if(!event){
                    res.status(404).json(`Event not found`);
                    return;
                }

                if(!event.managers.includes(user._id) || !event.participants.includes(user._id)){
                    res.status(403).json('You\'re not allowed create album in this event');
                    return;
                }

                let body = {
                    'event': event._id,
                    'createdBy': user._id
                };
                const albumModelQuery = new this.AlbumModel(body);
                albumModelQuery.save().then((album) => {
                    res.status(200).json(album || {});
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

            } catch(err) {
                console.error(`[ERROR] post:albums -> ${err}`);
            
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    showAlbum(){
        this.app.get('/albums/:id/show',async (req, res)=> {
            try{
                this.AlbumModel.findOne({ _id: req.params.id }).populate({
                    path: 'event',
                    populate: [
                        { 
                        path: 'createdBy', 
                        select:'email -_id' 
                        },
                        { 
                        path: 'managers', 
                        select:'email -_id' 
                        },
                        { 
                        path: 'participants', 
                        select:'email -_id' 
                        }
                    ]
                })
                .populate({
                    path: 'createdBy',
                    select: 'email -_id'
                })
                .populate({
                    path: 'pictures',
                    populate: [
                        { 
                        path: 'createdBy', 
                        select:'email -_id' 
                        },
                        { 
                            path: 'comments', 
                            select:'content -_id' 
                        },
                    ],
                    select: '-album'
                  })
                .then((album) => {
                    res.status(200).json(album || 'Album not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });
            } catch(err) {
                console.error(`[ERROR] get:album -> ${err}`);
            
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    updateAlbum(){
        this.app.put('/albums/:id/update', (req, res) => {
            try {
                    this.AlbumModel.findOneAndUpdate({_id: req.params.id}, 
                        req.body, {
                        // On renvoit l'album avec les nouvelles valeurs
                        new: true,
                        // Applique les règles de validation du modèle (Album)
                        runValidators: true
                    }).then((album) => {
                            res.status(200).json(album || 'Album not found');
                    }).catch((err)=>{  
                            res.status(422).json(err.message || {});
                    });
            }
            catch (err) {
                console.error(`[ERROR] put:album -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    deleteAlbum(){
        this.app.delete('/albums/:id/remove', async (req, res) => {
            try {
                const albumQuery = this.AlbumModel.findOne({ _id: req.params.id })
                .then((album) => {
                    return album;
                }).catch((err)=>{  
                    res.status(422).json(`Search album step : ${err.message}`  || {});
                });

                const album = await albumQuery;

                if(!album){
                    res.status(404).json('Album not found');
                    return;
                }

                // const pictureQuery = this.PictureModel.updateMany({ album: album._id })
                // .then((pictures) => {
                //     return pictures;
                // }).catch((err)=>{  
                //     res.status(422).json(`Search pictures step : ${err.message}`  || {});
                // });
                
                // // On récupère la liste des pictures pour d'abord supprimer les commentaires liés
                // let pictures = await pictureQuery;

                // if(pictures.length > 0){
                //     pictures.forEach(picture => {
                //         this.CommentModel.deleteMany({ type: picture._id, onModel: "Picture" }).catch((err)=>{
                //             res.status(422).json(`Remove comments of pictures: ${err.message}`  || {});
                //         });                   
                //     });
                // }
                this.CommentModel.deleteMany({ album: album._id, onModel: "Picture" }).catch((err)=>{
                    res.status(422).json(`Remove comments of album: ${err.message}`  || {});
                });

                this.PictureModel.deleteMany({ album: album._id  }).catch((err)=>{
                    res.status(422).json(`Remove pictures of album: ${err.message}`  || {});
                });

                this.AlbumModel.deleteOne({_id: album._id}).then((d)=>{
                    if (d.acknowledged && d.deletedCount == 1){
                        res.status(200).json('Album successfully delete');
                    }else{
                        res.status(200).json('Couldn\'t delete album');
                    }
                }).catch((err)=>{
                    res.status(422).json(`Remove album: ${err.message}`  || {});
                });
            }
            catch (err) {
                console.error(`[ERROR] delete:album -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    addPicture(){
        this.app.post('/albums/:id/picture/add', async (req, res) => {
            try {
                const albumQuery = this.AlbumModel.findOne({ _id: req.params.id }).then((album) => {
                    return album;
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

                const album = await albumQuery;
                if(!album){
                    res.status(404).json('Album not found');
                    return;
                }

                const userQuery = this.UserModel.findOne({ email: req.body.createdBy }).then((user) => {
                    return user;
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

                const user = await userQuery;
                if(!user){
                    res.status(404).json('User not found');
                    return;
                }

                const getEventQuery = this.EventModel.findOne({ _id: album.event }).then((event) => {
                    return event;
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                    return;
                });

                const event = await getEventQuery;
                if(!event.managers.includes(user._id) || !event.participants.includes(user._id)){
                    res.status(403).json('You\'re not allowed add picture inside this album');
                    return;
                }

                let body = {
                    'createdBy': user._id,
                    'album': album._id,
                    'image': req.body.image,
                    'description': req.body.description,
                    'event': album.event
                };
                const pictureModelQuery = new this.PictureModel(body);
                const pictureQuerySave = pictureModelQuery.save().then((picture) => {
                    return picture;
                }).catch((err)=>{  
                    return(err.message || {});
                });

                const picture = await pictureQuerySave;

                if(!picture._id){
                    res.status(422).json(picture);
                    return;
                }
                this.AlbumModel.findOneAndUpdate({_id: req.params.id}, 
                    {$push: { pictures: picture  }}, {
                    // On renvoit l'utilisateur avec les nouvelles valeurs
                    new: true,
                    // Applique les règles de validation du modèle (User)
                    runValidators: true
                }).then((album) => {
                        res.status(200).json(picture || {});
                }).catch((err)=>{  
                        res.status(422).json(err.message || {});
                });

            }
            catch (err) {
                console.error(`[ERROR] post:picture -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    showPictures(){
        this.app.get('/albums/:id/pictures/show', async (req, res) => {
            try {
                this.AlbumModel.findOne({ _id: req.params.id }, {pictures: 1, _id: 0})
                .populate({
                    path: 'pictures',
                    select: 'image description'
                    // select: 'image description -_id'
                  })
                .then((album) => {
                    res.status(200).json(album || 'Album not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

            }
            catch (err) {
                console.error(`[ERROR] get:pictures -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    updatePicture(){
        this.app.put('/picture/:id/update', (req, res) => {
            try {
                    // console.log(typeof req.body.managers);
                    this.PictureModel.findOneAndUpdate({_id: req.params.id}, 
                        req.body, {
                        // On renvoit la picture avec les nouvelles valeurs
                        new: true,
                        // Applique les règles de validation du modèle (Picture)
                        runValidators: true
                    }).then((picture) => {
                            res.status(200).json(picture || 'Picture not found');
                    }).catch((err)=>{  
                            res.status(422).json(err.message || {});
                    });
            }
            catch (err) {
                console.error(`[ERROR] put:picture -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    deletePicture(){
        this.app.delete('/albums/:idAlbum/picture/:idPicture/remove', async (req, res) => {
            try {
                const pictureQuery = this.PictureModel.findOne({ _id: req.params.idPicture })
                .then((picture) => {
                    return picture;
                }).catch((err)=>{  
                    res.status(422).json(`Search picture step : ${err.message}`  || {});
                });

                const picture = await pictureQuery;
                if(!picture){
                    res.status(404).json('Picture not found');
                    return;
                }

                const albumQuery =this.AlbumModel.findOneAndUpdate({_id: req.params.idAlbum }, 
                    {   
                        $pull: {
                        pictures: picture._id,
                        }
                    }, 
                    {
                        // On renvoit l'album avec les nouvelles valeurs
                        new: true,
                        // Applique les règles de validation du modèle (Album)
                        runValidators: true
                    }).then((album) => {
                            return album;
                    }).catch((err)=>{  
                        res.status(422).json(`Remove picture in album step : ${err.message}`  || {});
                    });

                    const album = await albumQuery;

                    if(!album){
                        res.status(404).json('Album not found');
                        return;
                    }
                    this.CommentModel.deleteMany({ type: picture._id, onModel: "Picture" }).catch((err)=>{
                        res.status(422).json(`Remove comments of picture: ${err.message}`  || {});
                    });

                    this.PictureModel.deleteOne({_id: picture._id}).then((d)=>{
                        if (d.acknowledged && d.deletedCount == 1){
                            res.status(200).json('Picture successfully delete');
                        }else{
                            res.status(200).json('Couldn\'t delete picture');
                        }
                    }).catch((err)=>{
                        res.status(422).json(`Remove picture: ${err.message}`  || {});
                    });
            }
            catch (err) {
                console.error(`[ERROR] delete:picture -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }


    addComment(){
        this.app.post('/albums/:idAlbum/picture/:idPicture/comment/add', async (req, res) => {
            try {
                const albumQuery = this.AlbumModel.findOne({ _id: req.params.idAlbum }).then((album) => {
                    return album;
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

                const album = await albumQuery;
                if(!album){
                    res.status(404).json('Album not found');
                    return;
                }

                const pictureQuery = this.PictureModel.findOne({ _id: req.params.idPicture }).then((picture) => {
                    return picture;
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

                const picture = await pictureQuery;
                if(!picture){
                    res.status(404).json('Picture not found');
                    return;
                }

                const userQuery = this.UserModel.findOne({ email: req.body.createdBy }).then((user) => {
                    return user;
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

                const user = await userQuery;
                if(!user){
                    res.status(404).json('User not found');
                    return;
                }

                if(req.body.onModel != 'Picture'){
                    res.status(422).json('Model of comment should be Picture');
                    return;
                }

                const getEventQuery = this.EventModel.findOne({ _id: album.event }).then((event) => {
                    return event;
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                    return;
                });
                const event = await getEventQuery;
                if(!event.managers.includes(user._id) || !event.participants.includes(user._id)){
                    res.status(403).json('You\'re not allowed add comment inside this picture');
                    return;
                }

                let body = {
                    'createdBy': user._id,
                    'content': req.body.content,
                    'type': picture._id,
                    'onModel': req.body.onModel,
                    'album': album._id,
                    'event': album.event
                };


                const commentModelQuery = new this.CommentModel(body);
                const commentQuerySave = commentModelQuery.save().then((comment) => {
                    return comment;
                }).catch((err)=>{  
                    return(err.message || {});
                });

                const comment = await commentQuerySave;
                if(!comment._id){
                    res.status(422).json(comment);
                    return;
                }

                this.PictureModel.findOneAndUpdate({_id: req.params.idPicture}, 
                    {$push: { comments: comment  }}, {
                    // On renvoit l'utilisateur avec les nouvelles valeurs
                    new: true,
                    // Applique les règles de validation du modèle (User)
                    runValidators: true
                }).then(() => {
                        res.status(200).json(comment || {});
                }).catch((err)=>{  
                        res.status(422).json(err.message || {});
                });
            }
            catch (err) {
                console.error(`[ERROR] post:comment -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    showCommentPicture(){
        this.app.get('/albums/:idAlbum/picture/:idPicture/comments', async (req, res) => {
            try {
                this.AlbumModel.findOne({ _id: req.params.idAlbum }, {event: 0, createdBy: 0, _id: 0, pictures: {$elemMatch: {$eq: req.params.idPicture}}})
                .populate({
                    path: 'pictures',
                    populate: [
                        { 
                            path: 'comments',
                            populate:{
                                path:'createdBy',
                                select:'email'
                                //select:'email -_id'
                            },
                            select:'content createdBy _id' 
                        },
                    ],
                    select: 'image description -_id'
                  })
                .then((album) => {
                    res.status(200).json(album || 'Album or picture not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });

            }
            catch (err) {
                console.error(`[ERROR] get:picturesComments -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    // updateComment(){
    //     this.app.put('/comment/:id', (req, res) => {
    //         try {
    //                 // console.log(typeof req.body.managers);
    //                 this.CommentModel.findOneAndUpdate({_id: req.params.id}, 
    //                     req.body, {
    //                     // On renvoit l'utilisateur avec les nouvelles valeurs
    //                     new: true,
    //                     // Applique les règles de validation du modèle (User)
    //                     runValidators: true
    //                 }).then((comment) => {
    //                         res.status(200).json(comment || 'Comment not found');
    //                 }).catch((err)=>{  
    //                         res.status(422).json(err.message || {});
    //                 });
    //         }
    //         catch (err) {
    //             console.error(`[ERROR] put:comment -> ${err}`);
    //             res.status(400).json({
    //                 code: 400,
    //                 message: `${err}`
    //             });
    //         }
    //     });
    // }

    deleteComment(){
        this.app.delete('/albums/:idAlbum/picture/:idPicture/comment/:idComment/remove', async (req, res) => {
            try {
                const commentQuery = this.CommentModel.findOne({ _id: req.params.idComment })
                .then((comment) => {
                    return comment;
                }).catch((err)=>{  
                    res.status(422).json(`Search comment step : ${err.message}`  || {});
                });
                const comment = await commentQuery;
                if(!comment){
                    res.status(404).json('Comment not found');
                    return;
                }
                const pictureQuery = this.PictureModel.findOneAndUpdate({_id: req.params.idPicture, album: req.params.idAlbum }, 
                    {   
                        $pull: {
                        comments: comment._id,
                        }
                    }, 
                    {
                        // On renvoit la picture avec les nouvelles valeurs
                        new: true,
                        // Applique les règles de validation du modèle (Picture)
                        runValidators: true
                    }).then((picture) => {
                            return picture;
                    }).catch((err)=>{  
                        res.status(422).json(`Remove comment in picture step : ${err.message}`  || {});
                    });

                const picture = await pictureQuery;
                if(!picture){
                    res.status(404).json('Picture not found');
                    return;
                }

                this.CommentModel.deleteOne({_id: comment._id}).then((d)=>{
                    if (d.acknowledged && d.deletedCount == 1){
                        res.status(200).json('Comment successfully delete');
                    }else{
                        res.status(200).json('Couldn\'t delete comment');
                    }
                }).catch((err)=>{
                    res.status(422).json(`Remove message: ${err.message}`  || {});
                });
            }
            catch (err) {
                console.error(`[ERROR] delete:comment -> ${err}`);
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });
    }

    run() {
        this.createAlbum();
        this.showAllAlbums();
        this.showAlbum();
        this.addPicture();
        this.showPictures();
        this.addComment();
        this.showCommentPicture();
        this.updateAlbum();
        this.updatePicture();
        this.deleteComment();
        this.deletePicture();
        this.deleteAlbum();
    }
}