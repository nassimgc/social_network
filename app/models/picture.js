const mongoose = require('mongoose');
const GUID = require('mongoose-guid')(mongoose);
const uniqueValidator = require('mongoose-unique-validator');
const UserModel = require('./user.js');

const Schema = new mongoose.Schema({
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User',
        immutable: true
    },
    image: {
      type: String,
      required: [true, 'Content is required'],
      validate: {
        validator: function(v) {
            var re = /[\/.](jpg|jpeg|png)$/i;
            return (!v || !v.trim().length) || re.test(v)
        },
        message: 'image format is invalid (jpg, jpeg, png).'
    },
    immutable: true
    },
    description: {
        type: String,
        required: [true, 'Description is required'],
      },
    comments: {
        type: [mongoose.Schema.Types.ObjectId],
        ref: 'Comment'
    },
    album:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Album',
        required: true,
        immutable: true
    },
    event:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Event',
        immutable: true
    }
}, {
    collection:'pictures',
    // Bien optimiser la requête
    minimize: false,
    versionKey: false
}).set('toJSON',{
    transform: (doc, ret) => {
        // doc : Données qu'on récupère
        ret.id = ret._id;

        delete ret._id;
    }
});

Schema.plugin(uniqueValidator, { message: '{PATH} already exist' });

module.exports = Schema;