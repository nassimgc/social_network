const Users = require('./users.js');
const Events = require('./events.js');
const Groups = require('./groups.js');
const Discussions = require('./discussions.js');
const Albums = require('./albums.js');
const Sondages = require('./sondages.js');

module.exports = {
    Users,
    Events,
    Groups,
    Discussions,
    Albums,
    Sondages
};